#ifndef DEF_MONSTER_GENERATOR
#define DEF_MONSTER_GENERATOR

#include <SFML/Graphics.hpp>

#include "Object.h"

class Ground;

class MonsterGenerator : public Object {

	//M�thodes
public:

	MonsterGenerator(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize,
		sf::Texture* texture, Ground* ground);
	MonsterGenerator();
	virtual ~MonsterGenerator();
	void action();

	//getter
	

	//setter
	


	//Attributs
protected:
	

};

#endif