#ifndef DEF_MONSTER
#define DEF_MONSTER

#define MONSTER_CHASE_DELAY 0.10
#define MONSTER_ACTION_DELAY 10.00
#define MONSTER_MOVE_DELAY 0.10
#define MONSTER_NEW_MOVE_DELAY 1.10




#include "DynamicEntity.h"

class Ground;


class Monster : public DynamicEntity {

	//M�thodes
public:
	Monster(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats, std::map<std::string,
		int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed, double posX,
		double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target, sf::Texture *texture,
		Ground* ground);
	virtual ~Monster();
	void spriteChanges(int col);
	virtual void move(const sf::Time& deltaTime);
	void tick(const sf::Time& deltaTime);
	virtual void moveToTarget();
	bool chase() const;

	//getter
	sf::Sprite getSprite() const;



	//Attributs
protected:
	bool mChase;

	

};

#endif
