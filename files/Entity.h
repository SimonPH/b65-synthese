#ifndef DEF_ENTITY
#define DEF_ENTITY

#define HITPOINT_REGEN 5
#define STAMINA_REGEN 5


#include <string>
#include <map>

#include <SFML/Graphics.hpp>

#include "Item.h"

class Ground;


class Entity {

	//M�thodes
public:

	Entity(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize, sf::Texture *texture, Ground* ground);
	Entity();
	virtual ~Entity();

	void drawSprite(sf::RenderWindow &window);


	//getter

	double getPosX() const;
	double getPosY() const;
	int getSpriteSize() const;
	double getSizeX() const;
	double getSizeY() const;
	sf::Sprite getSprite() const;
	std::string getName() const;

	//setter








	//Attributs
protected:
	Ground* mGround;
	sf::Texture *mTexture;
	sf::Sprite mSprite;
	std::string mName;
	double mPosX;
	double mPosY;
	int mSpriteSize;
	double mSizeX;
	double mSizeY;
};

#endif
