#include "Menu.h"


Menu::Menu(sf::RenderWindow& window) : mWindow{window}
{
	
}

//M�thodes


void Menu::makeTextZone(sf::Text& textZone, int fontSize, std::string text, int posX, int posY, int multiplierX, int multiplierY, sf::Color color) {

	textZone.setCharacterSize(fontSize);
	textZone.setFont(mFont);
	textZone.setString(text);
	textZone.setPosition(posX*multiplierX, posY*multiplierY);
	textZone.setColor(color);
}

Button* Menu::makeButton(Button* button, std::string text, sf::Vector2f size, int posX, int posY, int multiplierX, int multiplierY, sf::Color buttonColor, sf::Color borderColor, int fontSize, sf::Color color) {

	button = new Button(text, size, posX*multiplierX, posY*multiplierY);
	button->setBackgroundColor(buttonColor, borderColor);
	button->setFont(mFont, fontSize, color);

	return button;
}

//getter



//setter


