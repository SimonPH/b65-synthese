#ifndef DEF_SOUND_MANAGER
#define DEF_SOUND_MANAGER

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <map>
#include <string>


class SoundManager {

public:
	SoundManager();
	~SoundManager();

	//M�thodes
	void addSound(std::string, sf::Sound);

	//getter
	sf::Sound & getSound(std::string sound);

	//setter



	//Attributs
protected:
	std::map<std::string, sf::Sound> mSounds;

	sf::Sound mMainTheme;
	sf::SoundBuffer mMainThemeBuffer;

	sf::Sound mFountainSound;
	sf::SoundBuffer mFountainBuffer;

	sf::Sound mLevelupSound;
	sf::SoundBuffer mLevelupBuffer;

	sf::Sound mGenerateMonsterSound;
	sf::SoundBuffer mGenerateMonsterBuffer;

	sf::Sound mLoseSound;
	sf::SoundBuffer mLoseBuffer;

	sf::Sound mMeleeSound;
	sf::SoundBuffer mMeleeBuffer;

	sf::Sound mFireballSound;
	sf::SoundBuffer mFireballBuffer;

	sf::Sound mHealSound;
	sf::SoundBuffer mHealBuffer;

	sf::Sound mInvocationSound;
	sf::SoundBuffer mInvocationBuffer;


};

#endif

