#include "Missile.h"

Missile::Missile(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize, sf::Texture* texture,
	DynamicEntity *master, DynamicEntity *target, double speed, Ground* ground)
	: Projectile(name, posX, posY, sizeX, sizeY, spriteSize, texture, master, target, speed, ground)
{
	mSprite.setScale(0.35f, 0.35f);
	mSprite.setTexture(*mTexture);

}

Missile::~Missile() {


}


//M�thodes
void Missile::action() {
	if (mName == "Fireball") {
		releaseAttack();
	}
	else if (mName == "Heal") {
		healTarget();
	}

}


void Missile::releaseAttack() {


	int damage = mMaster->getDamage() + mMaster->getModifier()["Intelligence"];

	if (mTarget->getBlock())
	{
		mTarget->blockAttack(damage);
	}
	else {
		mTarget->armorBlock(damage);

	}

}

void Missile::healTarget() {

	int damage = mMaster->getDamage() + mMaster->getModifier()["Intelligence"];

	if (mTarget->getHp() + damage > mTarget->getStatsCap()["Vitality"]) {
		mTarget->setHitPoint(mTarget->getStatsCap()["Vitality"]);
	}
	else {
		mTarget->setHitPoint(mTarget->getHp() + damage);
	}


}

void Missile::goToTarget() {


	double  angle = atan2(mTarget->getPosY() - mPosY, mTarget->getPosX() - mPosX);
	mPosY += sin(angle) * mSpeed;
	mPosX += cos(angle) * mSpeed;

	mSprite.setPosition(mPosX, mPosY);


}

//getter



//setter


