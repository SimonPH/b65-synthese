#ifndef DEF_PLAYER
#define DEF_PLAYER
#define EXP 50
#define EXP_PER_KILL 25
#define MONEY_PER_KILL 15


#include <map>
#include <string>
#include <list>




#include "DynamicEntity.h"
#include "Monster.h"

class Ground;


class Player : public DynamicEntity{

	//M�thodes
	public:
		
		Player(int level, std::string name, int hitPoint, int stamina, std::map<std::string,
			int> stats, std::map<std::string, int> statsCap, std::map<std::string, int> modifier,
			std::map<std::string, Item*> equipment, double speed, double posX, double posY, double sizeX,
			double sizeY, int spriteSize, double range, double vision, DynamicEntity *target, sf::Texture *texture, Ground* ground);
		Player();
		virtual ~Player();
		void move(char direction);
		void attack();
		void walk();
		void run();
		void inRangeObject() const;
		void openLevelUpWindow();
		void receiveExpereience();
		void receiveGold();
		void checkLevelup();
		//Pourrai eventuellement �tre utilise
		//double lookOnMouse(sf::RenderWindow &window);




		//getter
		DynamicEntity* getTarget() const;
		int getExperience() const;
		int getExperienceCap() const;



		//setter
		void abandonTarget();
		void setTarget(Monster *potentialTarget);


		

	private:
	std::map<std::string, Item> mInventory;
	int mExperience;
	int mExperienceCap;





	//Attributs
	protected:

};

#endif