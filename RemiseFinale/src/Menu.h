#ifndef DEF_MENU
#define DEF_MENU

#include <SFML/Graphics.hpp>
#include "Button.h"

class Menu {

	//M�thodes
public:
	Menu(sf::RenderWindow& window);
	~Menu() = default;
	virtual short int event() = 0;
	virtual void draw() = 0;
	void makeTextZone(sf::Text& textZone, int fontSize, std::string text, int posX, int posY, int multiplierX, int multiplierY, sf::Color color);
	Button* makeButton(Button* button, std::string text, sf::Vector2f size, int posX, int posY, int multiplierX, int multiplierY, sf::Color buttonColor, sf::Color borderColor, int fontSize, sf::Color color);

	//getter


	//setter



	//Attributs
protected:
	sf::Font mFont;
	sf::RenderWindow& mWindow;

	

};

#endif
