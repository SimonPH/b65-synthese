#ifndef DEF_MISSILE
#define DEF_MISSILE


#include "Projectile.h"
#include "DynamicEntity.h"


class Missile : public Projectile {

	//M�thodes
public:
	Missile(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize, sf::Texture* texture,
		DynamicEntity  *master, DynamicEntity  *target, double speed, Ground* ground);
	virtual ~Missile();
	void releaseAttack();
	void action() override;
	void goToTarget() override;
	void healTarget();



	//getter
	
	

	//Attributs
protected:



};

#endif


