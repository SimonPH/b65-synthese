#ifndef DEF_HEALER_MONSTER
#define DEF_HEALER_MONSTER


#include "RangeMonster.h"
#include "Projectile.h"


class HealerMonster : public RangeMonster {

	//M�thodes
public:
	HealerMonster(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats, std::map<std::string,
		int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed, double posX,
		double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target, sf::Texture *texture,
		Ground* ground, sf::Texture *projectileTexture, sf::Texture *otherProjectileTexture,
		std::vector<Projectile*> & projectiles);
	virtual ~HealerMonster();
	void attack() override;
	void heal();
	bool inVision() override;
	void AlliesNeedLives();



	//getter


	//Attributs
protected:
	sf::Texture *mOtherProjectileTexture;

	

};

#endif

