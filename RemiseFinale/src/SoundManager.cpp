#include "SoundManager.h"


SoundManager::SoundManager()
{
	mMainThemeBuffer.loadFromFile("../sounds/mainTheme.ogg");
	mMainTheme.setBuffer(mMainThemeBuffer);
	mMainTheme.setVolume(10);
	mMainTheme.setLoop(true);
	addSound("mainTheme", mMainTheme);

	mFountainBuffer.loadFromFile("../sounds/replenish.wav");
	mFountainSound.setBuffer(mFountainBuffer);
	addSound("replenish", mFountainSound);

	mLevelupBuffer.loadFromFile("../sounds/levelup.wav");
	mLevelupSound.setBuffer(mLevelupBuffer);
	addSound("levelup", mLevelupSound);

	mGenerateMonsterBuffer.loadFromFile("../sounds/generateMonster.wav");
	mGenerateMonsterSound.setBuffer(mGenerateMonsterBuffer);
	addSound("generateMonster", mGenerateMonsterSound);

	mLoseBuffer.loadFromFile("../sounds/lose.ogg");
	mLoseSound.setBuffer(mLoseBuffer);
	addSound("lose", mLoseSound);

	mMeleeBuffer.loadFromFile("../sounds/melee.wav");
	mMeleeSound.setBuffer(mMeleeBuffer);
	addSound("melee", mMeleeSound);

	mFireballBuffer.loadFromFile("../sounds/fireball.wav");
	mFireballSound.setBuffer(mFireballBuffer);
	addSound("fireball", mFireballSound);

	mHealBuffer.loadFromFile("../sounds/heal.flac");
	mHealSound.setBuffer(mHealBuffer);
	addSound("heal", mHealSound);

	mInvocationBuffer.loadFromFile("../sounds/invocation.wav");
	mInvocationSound.setBuffer(mInvocationBuffer);
	addSound("invocation", mInvocationSound);

}

SoundManager::~SoundManager() {

}


//M�thodes
void SoundManager::addSound(std::string key, sf::Sound sound) {
	mSounds.insert(std::pair<std::string, sf::Sound>(key, sound));
}

//getter

sf::Sound & SoundManager::getSound(std::string sound)
{
	return mSounds.at(sound);
}

//setter


