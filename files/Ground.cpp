#include "Ground.h"
#include "RangeMonster.h"
#include "InvokerMonster.h"
#include "HealerMonster.h"
#include "MonsterGenerator.h"
#include "Fountain.h"



#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream> //print
#include <memory>
#include <algorithm>


Ground::Ground(int width, int height, sf::RenderWindow& window, SoundManager& soundManager, std::map<std::string, Item*> & equipment) : mMonsters{ }, mRangeMonsters{ },mEntities {  },
mProjectiles{ }, mPlayer{ }, mWidth{ width }, mHeight{ height }, mWindow{ window }, mSoundManager{ soundManager}, mEquipment {equipment}
{
	
}

Ground::~Ground() {


}


void Ground::addTexture(std::string key, sf::Texture* texture) {
	mMonstersTexture.insert(std::pair<std::string, sf::Texture*>(key, texture));
}

void Ground::freeMonters() {

	std::for_each(mMonsters.begin(), mMonsters.end(), [](Monster *m)->void {delete m; m = nullptr; });
	mMonsters.clear();

}

void Ground::freeEntities() {

	mEntities.clear();

}

void Ground::freeObjects() {

	mObjects.clear();

}

void Ground::freeFountains() {
	mFountains.clear();
}


void Ground::freeWalls() {
	mWalls.clear();
}


void Ground::freeProjectiles() {

	std::for_each(mProjectiles.begin(), mProjectiles.end(), [](Projectile *m)->void {delete m; m = nullptr; });
	mProjectiles.clear();

}

void Ground::relinkEntities() {

	mEntities.push_back(mPlayer);

	for (auto & o : mObjects)
	{
		mEntities.push_back(o);

	}

	for (auto & o : mWalls)
	{
		mEntities.push_back(&o);

	}



	
}

//le joueur obtient la cible la plus proche de lui si applicable dans sa distance
void Ground::obtainTarget() {
	if (!mMonsters.empty()) {
		Monster *potentialTarget = mMonsters.front();

		double pX{ pow(mPlayer->getPosX() - potentialTarget->getPosX(), 2) };
		double pY{ pow(mPlayer->getPosY() - potentialTarget->getPosY(), 2) };
		double potentialTargetDistance{ sqrt(pX + pY) };
		double tempDistance{ 0 };



		for (auto &m : mMonsters)
		{
			pX = pow(mPlayer->getPosX() - m->getPosX(), 2);
			pY = pow(mPlayer->getPosY() - m->getPosY(), 2);
			tempDistance = sqrt(pX + pY);
			if (tempDistance < potentialTargetDistance) {
				potentialTargetDistance = tempDistance;


				potentialTarget = m;

			}
		}

		if (potentialTargetDistance <= mPlayer->getVision()) {
			if (potentialTarget->isAlive()) {
				mPlayer->setTarget(potentialTarget);

			}
		}




	}
}

void Ground::refillPlayer() {
	mPlayer->setHitPoint(mPlayer->getStatsCap()["Vitality"]);
	mPlayer->setStamina(mPlayer->getStatsCap()["Endurance"]);

}

void Ground::refillFountain() {
	for (auto & f : mFountains)
	{
		f->refill();
	}
}


// gere les mouvements et les actions des projectiles
void Ground::manageProjectiles(const sf::Time& deltaTime) {
	
	for (int i = 0; i < mProjectiles.size(); i++)
	{
		Projectile* & p = mProjectiles.at(i);

		if (p->getTarget()->getPosX() > p->getPosX() - p->getSizeX() / 3 && p->getTarget()->getPosX() < p->getPosX() + p->getSizeX()) {
			if (p->getTarget()->getPosY() > p->getPosY() - p->getSizeY() && p->getTarget()->getPosY() < p->getPosY() + p->getSizeY()) {
				p->action();
				if (p->getName() != "Mana") {

					mProjectiles.erase(mProjectiles.begin() + i);
				}

			}
		}

		p->goToTarget();


		if (p->getName() == "Mana") {
			if (p->getMaster()->getPosX() > p->getPosX() - p->getSizeX() / 3 && p->getMaster()->getPosX() < p->getPosX() + p->getSizeX()) {
				if (p->getMaster()->getPosY() > p->getPosY() - p->getSizeY() && p->getMaster()->getPosY() < p->getPosY() + p->getSizeY()) {
					mProjectiles.erase(mProjectiles.begin() + i);

				}
			}
		}
		
	}

}

void Ground::spawnPlayer(std::map<std::string, Item*> equipment) {
	//ajout des stats de base dans un map
	std::map<std::string, int> stats = { { "Vitality" ,10 },{ "Strength" ,10 },{ "Agility" ,10 },{ "Intelligence" ,10 },{ "Endurance" ,10 } };
	std::map<std::string, int> modifier = { { "Vitality" ,0 },{ "Strength" ,0 },{ "Agility" ,0 },{ "Intelligence" ,0 },{ "Endurance" ,0 } };
	std::map<std::string, int> statsCap = { { "Vitality" ,BASE_HITPOINT + modifier["Vitality"]},{ "Intelligence" ,0 },{ "Endurance" , BASE_STAMINA + modifier["Endurance"] } };
	
	double posX{ 0.0 };
	double posY{0.0};

	do {
		posX = rand() % (mWidth - 200) + 100;
		posY = rand() % (mHeight - 200) + 100;
	} while (!checkSpawnCollision(posX, posY));

	//creation du joueur
	mPlayer = new Player(1, "Bob",statsCap["Vitality"], statsCap["Endurance"], stats, statsCap, modifier, equipment, 5.0, posX, posY, 22.0, 22.0, 64, 70.0, 160.0, nullptr, getMonstersTexture("player"), this);
	

	mEntities.push_back(mPlayer);



}

void Ground::spawnMonsters(std::map<std::string, Item*> equipment) {
	double sizeX{ 32.0 };
	double sizeY{ 32.0 };
	int spriteSize{ 64 };



	double posX{ 0.0 };
	double posY{0.0};
	int variety;
	int numberMonsters{ rand() % 5 + 2 };
	std::vector<std::map<std::string, int>> statsVector;



	for (int i = 0; i < numberMonsters; i++) {

		do {
			posX = rand() % (mWidth - 200) + 100;
			posY = rand() % (mHeight - 200) + 100;
		} while (!checkSpawnCollision(posX, posY));

		variety = rand() % 99 + 1;


		statsVector = countingStatsPoints();
		std::map<std::string, int> stat = statsVector[0];
		std::map<std::string, int> modifier = statsVector[1];
		std::map<std::string, int> statsCap = statsVector[2];
		int monsterLevel{ statsVector[3]["Level"] };


		std::string name;
		sf::Texture* texture;

		
		if (variety >= 1 && variety <= 40) {
			name = "Orc";
			texture = getMonstersTexture("orc");
			mMonsters.push_back(new Monster(monsterLevel, name, statsCap["Vitality"], statsCap["Endurance"], stat, statsCap, modifier, equipment, 10.0, posX, posY, sizeX, sizeY, spriteSize, 80.0, 170.0, mPlayer, texture, this));

		}
		else if(variety > 40 && variety <= 80 ){
			name = "Skeleton";
			texture = getMonstersTexture("skeleton");
			mMonsters.push_back(new Monster(monsterLevel, name, statsCap["Vitality"], statsCap["Endurance"], stat, statsCap, modifier, equipment, 10.0, posX, posY, sizeX, sizeY, spriteSize, 80.0, 170.0, mPlayer, texture, this));

		}
		else if (variety > 80 && variety <= 90) {
			name = "Elf mage";
			texture = getMonstersTexture("elf_mage");
			mMonsters.push_back(new RangeMonster(monsterLevel, name, statsCap["Vitality"], statsCap["Endurance"], stat, statsCap, modifier, equipment, 10.0, posX, posY, sizeX, sizeY, spriteSize, 200.0, 300.0, mPlayer, texture, this, getMonstersTexture("fireball"), mProjectiles));

		}
		else if (variety > 90 && variety <= 95) {
			name = "Human invoker";
			texture = getMonstersTexture("human_invoker");
			mMonsters.push_back(new InvokerMonster(monsterLevel, name, statsCap["Vitality"], statsCap["Endurance"], stat, statsCap, modifier, equipment, 10.0, posX, posY, sizeX, sizeY, spriteSize, 200.0, 300.0, mPlayer, texture, this, getMonstersTexture("skeleton")));
		}
		else if (variety > 95 && variety <= 100) {
			name = "Human healer";
			texture = getMonstersTexture("human_healer");
			mMonsters.push_back(new HealerMonster(monsterLevel, name, statsCap["Vitality"], statsCap["Endurance"], stat, statsCap, modifier, equipment, 10.0, posX, posY, sizeX, sizeY, spriteSize, 200.0, 300.0, mPlayer, texture, this, getMonstersTexture("lifeOrb"), getMonstersTexture("manaOrb"),  mProjectiles));
		}

		mEntities.push_back(mMonsters.back());


	}


}

void Ground::spawnMonsterGenerator() {
	double posX{ 0.0 };
	double posY{ 0.0 };

	do {
		posX = rand() % (mWidth - 200) + 100;
		posY = rand() % (mHeight - 200) + 100;
	} while (!checkSpawnCollision(posX, posY));

	mObjects.push_back(new MonsterGenerator("MonsterGenerator", posX, posY, 52.0, 32.0, 64, getMonstersTexture("large_headstone"),this));
	mEntities.push_back(mObjects.back());


}

void Ground::spawnFountains() {

	double posX{ 0.0 };
	double posY{ 0.0 };
	int foutainAmount{ 2 };
	bool life{ true };

	for (int i = 0; i < foutainAmount; i++) {

		do {
			posX = rand() % (mWidth - 200) + 100;
			posY = rand() % (mHeight - 200) + 100;
		} while (!checkSpawnCollision(posX, posY));

		if (life) {
			mObjects.push_back(new Fountain("lifeFoutain", posX, posY, 52.0, 32.0, 64, getMonstersTexture("lifeFountain"), this, true));
			life = false;
		}
		else {
			mObjects.push_back(new Fountain("manaFoutain", posX, posY, 52.0, 32.0, 64, getMonstersTexture("manaFountain"), this, false));
		}

		mFountains.push_back(dynamic_cast<Fountain*>(mObjects.back()));
		mEntities.push_back(mObjects.back());
	}



}


bool Ground::sameEntity(const Entity* me, const Entity* entity) {
	return me == entity;
}

bool Ground::checkCollision(const Entity* me, int tempPosX, int tempPosY) {

	for (const auto &e : mEntities) {

		if (!sameEntity(me, e)) {

			if (tempPosX > e->getPosX() - e->getSizeX() / 3 && tempPosX < e->getPosX() + e->getSizeX()) {
				if (tempPosY > e->getPosY() - e->getSizeY() && tempPosY < e->getPosY() + e->getSizeY()) {
					return false;
				}
			}

		}
	}



	return true;

}

// verefie lorsque les entites apparaissent de ne pas etre sur une autre entite
bool Ground::checkSpawnCollision(int tempPosX, int tempPosY) {

	for (const auto &e : mEntities) {


		if (tempPosX > e->getPosX() - e->getSizeX() / 3 && tempPosX < e->getPosX() + e->getSizeX()) {
			if (tempPosY > e->getPosY() - e->getSizeY() && tempPosY < e->getPosY() + e->getSizeY()) {
				return false;
			}
		}

	}



	return true;

}

//creation des murs autour de la carte
void Ground::spawnWallDelimiter() {
	double posX{ -40.0 }, posY{-80.0};
	double sizeX{ 140.0 }, sizeY{ 50.0 };
	int numberX = ceil(mWidth / sizeX);
	int numberY = ceil(mWidth / 80);
	sf::Texture* texture{ getMonstersTexture("boulder") };
	
	// mur du haut
	for (int i{ 0 }; i < numberX; i++) {
		mWalls.push_back(Entity("Wall", posX, posY, sizeX, sizeY, 64, texture,this));
		posX += sizeX;
	}
	

	
	//mur du bas
	posX = -40.0;
	posY = mHeight - 40.0;

	for (int i{ 0 }; i < numberX; i++) {
		mWalls.push_back(Entity("Wall", posX, posY, sizeX, sizeY, 64, texture,this));
		posX += sizeX;
	}

	//mur de gauche
	posX = -120.0;
	posY = 0;

	for (int i{ 0 }; i < numberY; i++) {
		mWalls.push_back(Entity("Wall", posX, posY, sizeX, sizeY, 64, texture,this));
		posY += 80;
	}

	//mur de droite
	posX = mWidth - 40.0;
	posY = 0;

	for (int i{ 0 }; i < numberY; i++) {
		mWalls.push_back(Entity("Wall", posX, posY, sizeX, sizeY, 64, texture,this));
		posY += 80;
	}

	
	for (auto & o : mWalls)
	{
		mEntities.push_back(&o);

	}
	
	

}

bool Ground::monstersIsAlive() {
	for (auto &m : mMonsters) {
		if (m->isAlive()) {
			return true;
		}
		

	}

	return false;
}





std::vector<std::map<std::string, int>> Ground::countingStatsPoints() {
	std::vector<std::map<std::string, int>> statsVector;

	std::map<std::string, int> stat = { { "Vitality" ,10 },{ "Strength" ,10 },{ "Agility" ,10 },{ "Intelligence" ,10 },{ "Endurance" ,10 } };

	int statsSize = stat.size();
	int statsPoints{ 0 };
	std::vector<int> spendPoints;
	int statValue{ 0 };
	int statSum{ 0 };

	int levelRange{ rand() % 2 + 1 };
	int level{ mPlayer->getLevel() + levelRange };


	if (level > 1) {
		statsPoints = mPlayer->getLevel() * statsSize;
	}
	else {
		statsPoints = 0;
	}

	if (level > 1) {
		for (int i{ 0 }; i < statsSize; i++) {
			statValue = rand() % statsPoints;
			spendPoints.push_back(statValue);

			if (i == statsSize - 1) {
				if (statsPoints - statSum > 0) {
					spendPoints.push_back(statsPoints - statSum);
				}
				else {
					spendPoints.push_back(0);
				}
			}
			statSum += statValue;
		}


		int statCounter{ 0 };
		for (auto & s : stat)
		{
			s.second += spendPoints[statCounter];
			statCounter += 1;

		}
	}
	
	std::map<std::string, int> modifier = { { "Vitality" ,0 },{ "Strength" ,0 },{ "Agility" ,0 },{ "Intelligence" ,0 },{ "Endurance" ,0 } };
	
	for (auto const& stat : stat)
	{
		if (stat.second > 10) {
			modifier[stat.first] = floor((stat.second - 10) / 2);
		}

	}

	std::map<std::string, int> statsCap = { { "Vitality" ,floor(BASE_HITPOINT * (1 +  modifier["Vitality"] * BASE_BONUS)) },{ "Intelligence" ,0 },{ "Endurance" , floor(BASE_STAMINA * (1 + modifier["Endurance"] * BASE_BONUS)) } };
	std::map<std::string, int> monsterLevel = { {"Level", level} };

	statsVector.push_back(stat);
	statsVector.push_back(modifier);
	statsVector.push_back(statsCap);
	statsVector.push_back(monsterLevel);

	return statsVector;

}



void Ground::draw() {
	drawMonsters();
	drawObjects();
	drawWalls();
	drawProjectiles();
	mPlayer->drawSprite(mWindow);
}


void Ground::drawMonsters() {
	
	for (auto &m : mMonsters) {
		m->drawSprite(mWindow);
	}

}

void Ground::drawObjects() {

	for (auto &o : mObjects) {
		o->drawSprite(mWindow);
	}
}

void Ground::drawWalls() {
	for (auto &w : mWalls) {
		w.drawSprite(mWindow);
	}
}


void Ground::drawProjectiles() {

	for (auto &p : mProjectiles) {
		p->drawSprite(mWindow);
	}
}

void Ground::monstersTick(const sf::Time& deltaTime) {
	
	for (auto &m : mMonsters) {
		if (m->isAlive()) {
			m->tick(deltaTime);
		}
		else {
			m->deathAnimation(deltaTime);

		}
		
	}

}

//getter

Player* & Ground::getPlayer() {
	return mPlayer;
}

std::list<Monster*> & Ground::getMonsters() {
	return mMonsters;
}

std::vector<RangeMonster> & Ground::getRangeMonsters() {
	return mRangeMonsters;
}

std::vector<Object*> & Ground::getObjects() {
	return mObjects;
}

std::vector<Entity> & Ground::getWalls() {
	return mWalls;
}

std::vector<Fountain*> & Ground::getFountains() {
	return mFountains;
}



std::list<Entity*> & Ground::getEntities() {
	return mEntities;
}

std::map<std::string, Item*> & Ground::getEquipment() {
	return mEquipment;
}


sf::Texture* Ground::getMonstersTexture(std::string monster) const
{
	return  mMonstersTexture.at(monster);
}

int Ground::getWidth() const {
	return mWidth;
}

int Ground::getHeight() const {
	return mHeight;
}

SoundManager & Ground::getSoundManager() {
	return mSoundManager;
}


std::vector<Projectile*> & Ground::getProjectiles() {
	return mProjectiles;
}






