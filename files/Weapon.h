#ifndef DEF_WEAPON
#define DEF_WEAPON

#include "Item.h"

class Weapon : public Item {

	//M�thodes
public:

	Weapon(int level, std::string type, std::string itemName, int durability, int damage, bool oneHanded);
	Weapon();
	virtual ~Weapon();


	//getter
	int getDamage() const;




	//Attributs
protected:
	int mDamage;
	bool mOneHanded;

};

#endif