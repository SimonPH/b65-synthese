#ifndef DEF_END_GAME_MENU
#define DEF_END_GAME_MENU

#include <SFML/Graphics.hpp>
#include "Menu.h"
#include "Button.h"
#include "Ground.h"
#include "Player.h"

class EndGameMenu : public Menu {

	//M�thodes
public:

	EndGameMenu(sf::RenderWindow& window, sf::RenderWindow& mainWindow, Ground &Ground, std::map<std::string, Item*> equipment, int width, int height);
	~EndGameMenu() = default;
	short int event();
	void draw();
	

	//getter


	//setter



	//Attributs
protected:

	std::map<std::string, Item*> mEquipment;
	Ground& mGround;
	int mWidth;
	int mHeight;
	sf::RenderWindow& mMainWindow;


	sf::Vector2i mMousePosition;


	sf::Texture mTexture;
	sf::Image mImage;
	sf::RectangleShape mBackground;

	sf::Text mEndMessage;

	Button* mButtonQuit;

	Button* mButtonTryAgain;

};

#endif
