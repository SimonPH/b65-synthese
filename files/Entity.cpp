#include "Entity.h"


Entity::Entity(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize, sf::Texture *texture, Ground* ground)
	: mName{ name }, mPosX{ posX }, mPosY{ posY }, mSizeX{ sizeX }, mSizeY{sizeY}, mSpriteSize {
	spriteSize}, mTexture{ texture }, mGround{ground}
{
	mSprite.setTexture(*mTexture);

}

Entity::Entity() {

}

Entity::~Entity() {

}


//M�thodes
void Entity::drawSprite(sf::RenderWindow &window) {
	mSprite.setPosition(mPosX, mPosY);
	window.draw(mSprite);
}

//getter
double Entity::getPosX() const {
	return mPosX;
}

double Entity::getPosY() const {
	return mPosY;
}

double Entity::getSizeX() const {
	return mSizeX;
}

double Entity::getSizeY() const {
	return mSizeY;
}


int Entity::getSpriteSize() const {
	return mSpriteSize;
}

std::string Entity::getName() const {
	return mName;
}

sf::Sprite Entity::getSprite() const {
	return mSprite;
}