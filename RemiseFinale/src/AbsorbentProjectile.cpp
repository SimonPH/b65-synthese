#include "AbsorbentProjectile.h"


AbsorbentProjectile::AbsorbentProjectile(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize, sf::Texture* texture,
	DynamicEntity *master, DynamicEntity *target, double speed, Ground* ground)
	: Projectile(name, posX, posY, sizeX, sizeY, spriteSize, texture, master, target, speed, ground)
{
	mSprite.setScale(0.35f, 0.35f);
	mSprite.setTexture(*mTexture);

}

AbsorbentProjectile::~AbsorbentProjectile() {


}


//M�thodes
void AbsorbentProjectile::action() {
	
	stealStaminaTarget();
}

//vole l'endurance de la cible
void AbsorbentProjectile::stealStaminaTarget() {

	int damage = mMaster->getDamage() + mMaster->getModifier()["Intelligence"];
	

	if (mTarget->getStamina() - damage > 0) {

		mTarget->setStamina(mTarget->getStamina() - damage);
	}
	else {
		damage = abs(mTarget->getStamina() - damage);
		mTarget->setStamina(0);
		mTarget->setHitPoint(mTarget->getHp() - damage);
	}
}

//le projectile va vers la cible
void AbsorbentProjectile::goToTarget() {

	
	double  angle = atan2(mMaster->getPosY() - mPosY, mMaster->getPosX() - mPosX);
	mPosY += sin(angle) * mSpeed;
	mPosX += cos(angle) * mSpeed;

	mSprite.setPosition(mPosX, mPosY);


}

//getter



//setter


