#include "HealerMonster.h"
#include "Missile.h"
#include "AbsorbentProjectile.h"
#include "Ground.h"

#include <SFML/Graphics.hpp>


HealerMonster::HealerMonster(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats,
	std::map<std::string, int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed,
	double posX, double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target,
	sf::Texture *texture, Ground* ground, sf::Texture *projectileTexture, sf::Texture *otherProjectileTexture, std::vector<Projectile*> & projectiles)
	: RangeMonster(level, name, hitPoint, stamina, stats, statsCap, modifier, equipment, speed, posX, posY, sizeX, sizeY, spriteSize,
		range, vision, target, texture, ground, projectileTexture, projectiles), mOtherProjectileTexture{otherProjectileTexture}
{

	mSprite.setTexture(*mTexture);

	mRect.left = 0;
	mRect.top = spriteSize * 10;
	mRect.height = spriteSize;
	mRect.width = spriteSize;

	mSprite.setTextureRect(mRect);


}

HealerMonster::~HealerMonster() {


}

void HealerMonster::AlliesNeedLives() {

	if (!mGround->getMonsters().empty()) {
		Monster *potentialTarget = mGround->getMonsters().front();

		double pX{ pow(mPosX - potentialTarget->getPosX(), 2) };
		double pY{ pow(mPosY - potentialTarget->getPosY(), 2) };
		double potentialTargetDistance{ sqrt(pX + pY) };
		double tempDistance{ 0 };
		int tempHitPoint{ 0 };
		int potentialTargetHitPoint{ potentialTarget->getHp() };



		for (auto &m : mGround->getMonsters())
		{
			if (m->isAlive()) {
				if (m->getHp() != m->getStatsCap()["Vitality"]) {

					pX = pow(mPosX - m->getPosX(), 2);
					pY = pow(mPosY - m->getPosY(), 2);
					tempDistance = sqrt(pX + pY);
					tempHitPoint = m->getHp();
					if (tempHitPoint == potentialTargetHitPoint)
					{
						if (tempDistance < potentialTargetDistance) {
							potentialTargetDistance = tempDistance;


							potentialTarget = m;

						}
					}
					else if (tempHitPoint < potentialTargetHitPoint) {
						potentialTarget = m;

					}
				}
				
			}
		}

		if (potentialTarget->getHp() != potentialTarget->getStatsCap()["Vitality"]) {
			mTarget = potentialTarget;
		}
		else {
			mTarget = mGround->getPlayer();
		}

	}
}

void HealerMonster::attack() {

	AlliesNeedLives();

	setMoving(false);
	setAttacking(true);
	setBlocking(false);

	if (mGround->sameEntity(mTarget, mGround->getPlayer()))
	{
		mGround->getSoundManager().getSound("heal").stop();
		mGround->getSoundManager().getSound("heal").play();
		mProjectiles.push_back(new AbsorbentProjectile("Mana", mTarget->getPosX(), mTarget->getPosY(), 10, 10, 64, mOtherProjectileTexture, this, mTarget, 8.0, mGround));
	}
	else {
		if (mTarget->isAlive()) {
			heal();
		}
	}


}

void HealerMonster::heal() {

	if (enoughEnergy(ATTACK_COST)) {
		mGround->getSoundManager().getSound("heal").stop();
		mGround->getSoundManager().getSound("heal").play();
		mStamina -= ATTACK_COST;
		setMoving(false);
		setAttacking(true);
		setBlocking(false);

		

		mProjectiles.push_back(new Missile("Heal", mPosX, mPosY, 10, 10, 64, mProjectileTexture, this, mTarget, 8.0, mGround));

	}

}

bool HealerMonster::inVision() {

	AlliesNeedLives();

	double distance{ ((mPosX - mTarget->getPosX())*(mPosX - mTarget->getPosX())) + ((mPosY - mTarget->getPosY())*(mPosY - mTarget->getPosY())) };
	distance = sqrt(distance);

	if (distance <= mVision) {
		return true;
	}

	if (!mGround->sameEntity(mTarget, mGround->getPlayer())) {
		return true;

	}


	return false;


}

//getter


