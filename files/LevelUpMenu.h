#ifndef DEF_LEVELUP_MENU
#define DEF_LEVELUP_MENU

#include <SFML/Graphics.hpp>
#include "Menu.h"
#include "Button.h"
#include "Player.h"

class LevelUpMenu : public Menu {

	//M�thodes
public:

	LevelUpMenu(sf::RenderWindow& window, Player& player);
	~LevelUpMenu() = default;
	short int event();
	void draw();
	void lessStats(Button* button, sf::Vector2i mousePosition, sf::Text& text, std::string stat);
	void moreStats(Button* button, sf::Vector2i mousePosition, sf::Text& text);
	void setStats();
	bool isValid();

	//getter
	

	//setter
	


	//Attributs
protected:

	Player& mPlayer;
	std::map<std::string, int> mStats;


	sf::Vector2i mMousePosition;



	sf::Texture mTexture;
	sf::Image mImage;
	sf::RectangleShape mBackground;

	sf::Text mStatsPointsText;
	sf::Text mVitalityText;
	sf::Text mStrengthText;
	sf::Text mAgilityText;
	sf::Text mIntelligenceText;
	sf::Text mEnduranceText;

	sf::Text mStatsPointsTextValue;
	sf::Text mVitalityTextValue;
	sf::Text mStrengthTextValue;
	sf::Text mAgilityTextValue;
	sf::Text mIntelligenceTextValue;
	sf::Text mEnduranceTextValue;

	Button* mButtonConfirm;

	Button* mButtonMoreVitality;
	Button* mButtonMoreStrength;
	Button* mButtonMoreAgility;
	Button* mButtonMoreIntelligence;
	Button* mButtonMoreEndurance;

	Button* mButtonLessVitality;
	Button* mButtonLessStrength;
	Button* mButtonLessAgility;
	Button* mButtonLessIntelligence;
	Button* mButtonLessEndurance;

};

#endif