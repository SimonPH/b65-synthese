#include "InterfaceManager.h"
#include "Ground.h"


InterfaceManager::InterfaceManager(sf::RenderWindow& window, Ground* ground) : mWindow{ window }, mGround{ground}
{
	if (!mFont.loadFromFile("../fonts/ringbearer.ttf"))
	{
		// error...
	}

	mTargetName.setFont(mFont);
	mTargetName.setCharacterSize(20);
	mTargetName.setPosition(800, 40);

	mTargetLevel.setFont(mFont);
	mTargetLevel.setCharacterSize(20);
	mTargetLevel.setPosition(800, 65);

	mTargetHitPoint.setFont(mFont);
	mTargetHitPoint.setCharacterSize(20);
	mTargetHitPoint.setPosition(800, 90);

	mTargetStamina.setFont(mFont);
	mTargetStamina.setCharacterSize(20);
	mTargetStamina.setPosition(800, 115);

	mTextFps.setFont(mFont);
	mTextFps.setCharacterSize(20);
	mTextFps.setPosition(1300, 40);

	mPlayerName.setFont(mFont);
	mPlayerName.setCharacterSize(20);
	mPlayerName.setPosition(50, 40);

	mPlayerLevel.setFont(mFont);
	mPlayerLevel.setCharacterSize(20);
	mPlayerLevel.setPosition(50, 65);

	mPlayerHitPoint.setFont(mFont);
	mPlayerHitPoint.setCharacterSize(20);
	mPlayerHitPoint.setPosition(50, 90);

	mPlayerStamina.setFont(mFont);
	mPlayerStamina.setCharacterSize(20);
	mPlayerStamina.setPosition(50, 115);

	mPlayerExperience.setFont(mFont);
	mPlayerExperience.setCharacterSize(20);
	mPlayerExperience.setPosition(50, 140);

	mPlayerNameLabel.setFont(mFont);
	mPlayerNameLabel.setCharacterSize(15);

	mEnemyCircle.setOutlineThickness(5);

}

InterfaceManager::~InterfaceManager() {

}


//M�thodes
void InterfaceManager::changeText(float fps) {

	mTextFps.setString("FPS : " + std::to_string(fps));

	mPlayerName.setString("Nom : " + mGround->getPlayer()->getName());

	mPlayerLevel.setString("Niveau : " + std::to_string(mGround->getPlayer()->getLevel()));

	mPlayerHitPoint.setString("Point de vie : " + std::to_string(mGround->getPlayer()->getHp()));

	mPlayerStamina.setString("Endurance : " + std::to_string(mGround->getPlayer()->getStamina()));
	
	mPlayerExperience.setString("Experience : " + std::to_string(mGround->getPlayer()->getExperience()) + " / " + std::to_string(mGround->getPlayer()->getExperienceCap()));
	
	mPlayerNameLabel.setString(mGround->getPlayer()->getName());
	mPlayerNameLabel.setPosition(mGround->getPlayer()->getPosX() + 15, mGround->getPlayer()->getPosY() - 20);


	if (mGround->getPlayer()->getTarget() != nullptr)
	{

		mTargetName.setString("Cible : " + mGround->getPlayer()->getTarget()->getName());

		mTargetLevel.setString("Niveau : " + std::to_string(mGround->getPlayer()->getTarget()->getLevel()));

		mTargetHitPoint.setString("Point de vie : " + std::to_string(mGround->getPlayer()->getTarget()->getHp()));

		mTargetStamina.setString("Endurance : " + std::to_string(mGround->getPlayer()->getTarget()->getStamina()));

		mEnemyCircle.setRadius(mGround->getPlayer()->getTarget()->getSpriteSize() / 4);
		if (mGround->getPlayer()->inRange()) {
			mEnemyCircle.setFillColor(sf::Color(255, 0, 0, 200));
			mEnemyCircle.setOutlineColor(sf::Color(255, 0, 0, 255));


		}
		else {
			mEnemyCircle.setFillColor(sf::Color(0, 0, 225, 200));
			mEnemyCircle.setOutlineColor(sf::Color(0, 0, 255, 255));


		}
		mEnemyCircle.setPosition(mGround->getPlayer()->getTarget()->getPosX() + 20, mGround->getPlayer()->getTarget()->getPosY() + 50);


	}
	else {
		mTargetName.setString("Pas de cible");
		mTargetLevel.setString("");
		mTargetHitPoint.setString("");
		mTargetStamina.setString("");
		mEnemyCircle.setFillColor(sf::Color(0, 0, 225, 0));
		mEnemyCircle.setOutlineColor(sf::Color(0, 0, 255, 0));


	}
	
}

void InterfaceManager::drawText() {

	mWindow.draw(mEnemyCircle);
	
	//dessine les textes
	mWindow.draw(mTextFps);
	mWindow.draw(mPlayerName);
	mWindow.draw(mPlayerLevel);
	mWindow.draw(mPlayerHitPoint);
	mWindow.draw(mPlayerStamina);
	mWindow.draw(mPlayerExperience);


	mWindow.draw(mTargetName);
	mWindow.draw(mTargetLevel);
	mWindow.draw(mTargetHitPoint);
	mWindow.draw(mTargetStamina);



	//nom au dessus du joueur
	mWindow.draw(mPlayerNameLabel);

}


//getter



//setter


