#ifndef DEF_INTERFACE_MANAGER
#define DEF_INTERFACE_MANAGER

#include <SFML/Graphics.hpp>

class Ground;

class InterfaceManager {

public:
	InterfaceManager(sf::RenderWindow& window, Ground* ground);
	~InterfaceManager();

	//M�thodes
	void changeText(float fps);
	void drawText();
	//getter


	//setter



	//Attributs
protected:
	Ground* mGround;
	sf::RenderWindow& mWindow;
	sf::Font mFont;
	sf::Text mTextFps;
	sf::Text mPlayerName;
	sf::Text mPlayerLevel;
	sf::Text mPlayerHitPoint;
	sf::Text mPlayerStamina;
	sf::Text mPlayerExperience;
	sf::Text mPlayerNameLabel;
	sf::Text mTargetName;
	sf::Text mTargetLevel;
	sf::Text mTargetHitPoint;
	sf::Text mTargetStamina;
	sf::CircleShape mEnemyCircle;



};

#endif

