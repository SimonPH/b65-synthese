#ifndef DEF_TILEMAP
#define DEF_TILEMAP

#include <SFML/Graphics.hpp>


class TileMap : public sf::Drawable, public sf::Transformable {

	//M�thodes
public:
	TileMap();
	~TileMap() = default;
	bool load(const std::string& tileset, sf::Vector2u tileSize,  int* tiles, int width, int height);

	


	//Attributs
private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	sf::VertexArray mVertexArray;
	sf::Texture mTileTexture;

};

#endif
