#ifndef DEF_MINION
#define DEF_MINION


#include "Monster.h"
#include "Projectile.h"



class Minion : public Monster {

	//M�thodes
public:
	Minion(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats, std::map<std::string,
		int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed, double posX,
		double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target, sf::Texture *texture,
		Ground* ground ,DynamicEntity* mMaster, double posXToMaster, double posYToMaster);
	virtual ~Minion();
	bool inVision() override;
	void moveToMaster();

	//setter
	void setPosToMaster(double x, double y);

	//getter
	DynamicEntity* getMaster() const;

	


	//Attributs
protected:
	DynamicEntity* mMaster;
	double mPosXToMaster;
	double mPosYToMaster;

};

#endif

