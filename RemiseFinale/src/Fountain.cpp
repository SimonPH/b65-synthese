#include "Fountain.h"
#include "Ground.h"

Fountain::Fountain(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize,
	sf::Texture* texture, Ground* ground, bool life) : Object(name, posX, posY, sizeX, sizeY, spriteSize, texture, ground), mFull{true}, mLife {life}
{
	mSprite.setTexture(*mTexture);

}

Fountain::Fountain() {


}

Fountain::~Fountain() {


}

//M�thodes

//action de donner de la vie ou de l'endurance au joueur
void Fountain::action() {

	if (mLife) {
		if (mFull) {
			if (mGround->getPlayer()->getHp() != mGround->getPlayer()->getStatsCap()["Vitality"]) {
				mGround->getPlayer()->setHitPoint(mGround->getPlayer()->getStatsCap()["Vitality"]);
				mFull = false;
				mSprite.setTexture(*mGround->getMonstersTexture("emptyFountain"));
				mGround->getSoundManager().getSound("replenish").play();

			}

		}
	}
	else{
		if (mFull) {
			if (mGround->getPlayer()->getStamina() != mGround->getPlayer()->getStatsCap()["Endurance"]) {
				mGround->getPlayer()->setStamina(mGround->getPlayer()->getStatsCap()["Endurance"]);
				mFull = false;
				mSprite.setTexture(*mGround->getMonstersTexture("emptyFountain"));
				mGround->getSoundManager().getSound("replenish").play();


			}
		}
	}


}

void Fountain::refill() {

	mFull = true;

	if (mLife) {
		mSprite.setTexture(*mGround->getMonstersTexture("lifeFountain"));

	}
	else {
		mSprite.setTexture(*mGround->getMonstersTexture("manaFountain"));

	}



}


//getter


//setter


