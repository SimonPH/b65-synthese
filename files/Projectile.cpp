#include "Projectile.h"

Projectile::Projectile(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize, sf::Texture* texture,
	DynamicEntity *master, DynamicEntity *target, double speed, Ground* ground)
	: Entity(name, posX, posY, sizeX, sizeY, spriteSize, texture, ground), mMaster{master}, mTarget {target}, mSpeed{ speed }
{
	mSprite.setScale(0.35f, 0.35f);
	mSprite.setTexture(*mTexture);

}

Projectile::~Projectile() {


}


//M�thodes

void Projectile::action() {

}

void Projectile::goToTarget() {

}


//getter
DynamicEntity* & Projectile::getTarget() {
	return mTarget;
}

DynamicEntity* & Projectile::getMaster() {
	return mMaster;
}


//setter


