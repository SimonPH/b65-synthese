#ifndef DEF_PROJECTILE
#define DEF_PROJECTILE


#include "Entity.h"
#include "DynamicEntity.h"


class Projectile : public Entity {

	//M�thodes
public:
	Projectile(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize, sf::Texture* texture,
		DynamicEntity  *master, DynamicEntity  *target, double speed, Ground* ground);
	virtual ~Projectile();
	virtual void action();
	virtual void goToTarget();



	//getter
	DynamicEntity* & getTarget();
	DynamicEntity* & getMaster();

	

	//Attributs
protected:

	DynamicEntity *mTarget;
	DynamicEntity  *mMaster;
	double mSpeed;



};

#endif

