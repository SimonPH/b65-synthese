#ifndef DEF_RANGE_MONSTER
#define DEF_RANGE_MONSTER


#include "Monster.h"
#include "Projectile.h"



class RangeMonster : public Monster {

	//M�thodes
public:
	RangeMonster(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats, std::map<std::string,
		int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed, double posX,
		double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target, sf::Texture *texture,
		Ground* ground, sf::Texture *projectileTexture,
		std::vector<Projectile*> & projectiles);
	virtual ~RangeMonster();
	virtual void attack() override;
	
	//getter
	std::vector<Projectile*> & getProjectiles();


	//Attributs
protected:
	std::vector<Projectile*> & mProjectiles;
	sf::Texture *mProjectileTexture;

};

#endif
