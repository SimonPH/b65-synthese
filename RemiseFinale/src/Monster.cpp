#include "Monster.h" 

#include <SFML/Graphics.hpp>
#include "Ground.h"


Monster::Monster(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats,
	std::map<std::string, int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed,
	double posX, double posY, double sizeX , double sizeY, int spriteSize, double range, double vision, DynamicEntity *target,
	sf::Texture *texture, Ground* ground)
	: DynamicEntity(level, name, hitPoint, stamina, stats, statsCap, modifier, equipment, speed, posX, posY, sizeX, sizeY,
		spriteSize, range, vision, target, texture, ground), mChase { false}
{
	
	mSprite.setScale(1.15f, 1.15f);
	mSprite.setTexture(*mTexture);

	mRect.left = 0;
	mRect.top = spriteSize * 10;
	mRect.height = spriteSize;
	mRect.width = spriteSize;

	mSprite.setTextureRect(mRect);


}

Monster::~Monster() {


}


void Monster::spriteChanges(int col) {

	if (mRect.left == mSpriteSize * col)
		mRect.left = 0;
	else
		mRect.left += mSpriteSize;

	mSprite.setTextureRect(mRect);
	
}

void Monster::move(const sf::Time& deltaTime){
	if (!chase()) {
		
		mMoving = true;

		double tempPosX{ mPosX };
		double tempPosY{ mPosY };
		static double lastPosX{ 0.0 };
		static double lastPosY{ 0.0 };
		static double targetPosX{ 0.0 };
		static double targetPosY{ 0.0 };
		static double accumulatedTimeNewMove{ 0 };
		accumulatedTimeNewMove += deltaTime.asSeconds();

		if (lastPosX == tempPosX && lastPosY == tempPosY) {
			targetPosX = 0.0;
			targetPosY = 0.0;
		}

		if (targetPosX == 0.0 && targetPosY == 0.0) {
			if (accumulatedTimeNewMove > MONSTER_NEW_MOVE_DELAY) {

				double radius{ rand() % 599 + 200.0 };
				double angle{ rand() % 359 + 1.0 };

				targetPosX = tempPosX + radius * cos(angle);
				targetPosY = tempPosY + radius * sin(angle);

				accumulatedTimeNewMove = 0;
			}
		}
		if (targetPosX != 0.0 && targetPosY != 0.0) {

			if (mPosX < targetPosX)
			{
				tempPosX += mSpeed;
				setRectRow(11);

				if (mPosY < targetPosY)
				{
					tempPosY += mSpeed;
					//setRectRow(10);
				}
				else {
					tempPosY -= mSpeed;
					//setRectRow(8);
				}
			}
			else {
				tempPosX -= mSpeed;
				setRectRow(9);

				if (mPosY < targetPosY)
				{
					tempPosY += mSpeed;
					//setRectRow(10);
				}
				else {
					tempPosX -= mSpeed;
					//setRectRow(8);
				}
			}

			if (isMoving()) {

				if (mGround->checkCollision(this, tempPosX, tempPosY)) {
					if (inLimitX(tempPosX)) {
						mPosX = tempPosX;
						lastPosX = tempPosX;

					}
					else {
						setMoving(false);
						targetPosX = 0.0;
						targetPosY = 0.0;
					}

					if (inLimitY(tempPosY)) {
						mPosY = tempPosY;
						lastPosY = tempPosY;
						targetPosX = 0.0;
						targetPosY = 0.0;

					}
					else {
						setMoving(false);

					}

				}
				else {
					targetPosX = 0.0;
					targetPosY = 0.0;
				}




				//setRectRow(row);
				spriteChanges(8);

				mSprite.setPosition(mPosX, mPosY);

			}
		}
		

	}

}


void Monster::tick(const sf::Time& deltaTime) {
	
	static double accumulatedTimeChase{ 0 };
	accumulatedTimeChase += deltaTime.asSeconds();

	static double accumulatedTimeAction{ 0 };
	accumulatedTimeAction += deltaTime.asSeconds();

	static double accumulatedTimeMove{ 0 };
	accumulatedTimeMove += deltaTime.asSeconds();

	hitPointRegeneration(deltaTime);
	staminaRegeneration(deltaTime);

	if(!inRange()) {
		setAttacking(false);
		setBlocking(false);
		if (accumulatedTimeMove > MONSTER_MOVE_DELAY) {

			move(deltaTime);
			accumulatedTimeMove = 0;

		}
	}
	else {

		if (accumulatedTimeAction > MONSTER_ACTION_DELAY) {


			int action{ rand() % 2 + 1 };


			if (action == 1)
			{
				
				attack();


			}
			else if (action == 2) {
				setAttacking(false);
				setBlocking(true);

			}



			accumulatedTimeAction = 0;

		}

		//attackAnimation(deltaTime);
		//blockAnimation(deltaTime);
	}

	

	if (accumulatedTimeChase > MONSTER_CHASE_DELAY) {
		if (inVision()) {
			moveToTarget();
		}
		else {
			mChase = false;
			//setRectCol(0);
		}
		accumulatedTimeChase = 0;
	}

}


void Monster::moveToTarget()  {

	double tempPosX{ mPosX };
	double tempPosY{ mPosY };

	if (!inRange()) {
		mChase = true;
		

		if (mPosX < mTarget->getPosX())
		{
			tempPosX += mSpeed;
			setRectRow(11);

			if (mPosY < mTarget->getPosY())
			{
				tempPosY += mSpeed;
				//setRectRow(10);
			}
			else {
				tempPosY -= mSpeed;
				//setRectRow(8);
			}
		}
		else {
			tempPosX -= mSpeed;
			setRectRow(9);

			if (mPosY < mTarget->getPosY())
			{
				tempPosY += mSpeed;
				//setRectRow(10);
			}
			else {
				tempPosX -= mSpeed;
				//setRectRow(8);
			}
		}

		if (mGround->checkCollision(this, tempPosX, tempPosY)) {
			if (inLimitX(tempPosX)) {
				mPosX = tempPosX;


			}
			

			if (inLimitY(tempPosY)) {
				mPosY = tempPosY;

			}
			

		}
		

		mSprite.setPosition(mPosX, mPosY);
		spriteChanges(8);
	}
	else {
		//setRectCol(0);
	}

	

}

bool Monster::chase() const {
	return mChase;
}




sf::Sprite Monster::getSprite() const {
	return mSprite;
}
