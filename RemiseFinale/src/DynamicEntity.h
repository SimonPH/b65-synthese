#ifndef DEF_DYNAMIC_ENTITY
#define DEF_DYNAMIC_ENTITY

#define ATTACK_COST 5
#define BLOCK_COST 5
#define HITPOINT_REGEN 5
#define STAMINA_REGEN 5
#define BASE_HITPOINT 100
#define BASE_STAMINA 100
#define HITPOINT_REGEN_DELAY 10.0
#define STAMINA_REGEN_DELAY 1.0
#define SPRITE_CHANGE_DELAY 0.10
#define ATTACK_ANIMATION_DELAY 0.05
#define BLOCK_ANIMATION_DELAY 0.10
#define DEATH_ANIMATION_DELAY 0.20




#include <string>
#include <map>
#include<list>

#include <SFML/Graphics.hpp>


#include "Entity.h"
#include "Item.h"


class DynamicEntity : public Entity {

//M�thodes
public:

	DynamicEntity(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats, std::map<std::string,
		int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed, double posX,
		double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target, sf::Texture *texture,
		Ground* ground);
	DynamicEntity();
	virtual ~DynamicEntity();

	virtual void attack();
	void attackAnimation(const sf::Time& deltaTime);
	void blockAnimation(const sf::Time& deltaTime);
	void deathAnimation(const sf::Time& deltaTime);
	void receiveDamage(int takenDamages);
	void blockAttack(int takenDamages);
	void armorBlock(int takenDamages);
	void hitPointRegeneration(const sf::Time& deltaTime);
	void staminaRegeneration(const sf::Time& deltaTime);
	void setStatsModifier();
	void setStatsCap();
	virtual bool isAlive();
	bool inLimitX(double tempPosX);
	bool inLimitY(double tempPosY);
	virtual bool inVision();
	bool inRange() const;
	bool enoughEnergy(int cost) const;
	void spriteChanges(int col, const sf::Time& deltaTime);
	bool isMoving() const;
	bool isBlocking() const;
	bool isAttacking() const;


	//getter
	
	int getHp() const;
	int getStamina() const;
	int getArmorDefense() const;
	int getDamage() const;
	double getSpeed() const;
	int getBlockValue() const;
	bool getBlock() const;
	double getRange() const;
	double getVision() const;
	sf::IntRect getRect() const;
	int getLevel() const;
	std::map<std::string, int> getStats() const;
	std::map<std::string, int> getStatsCap() const;
	std::map<std::string, int> getModifier() const;
	DynamicEntity* getTarget() const;
	std::map<std::string, Item*> getEquipment() const;

	

	//setter
	void setRectRow(int row);
	void setRectCol(int col);
	void setMoving(bool move);
	void setBlocking(bool block);
	void setAttacking(bool attack);
	void setStats(std::map<std::string, int> stats);
	void setPos(double x, double y);
	void setHitPoint(int hitPoint);
	void setStamina(int stamina);






	




//Attributs
protected:
	DynamicEntity *mTarget;
	sf::IntRect mRect;
	int mLevel;
	int mHitPoint;
	int mStamina;
	std::map<std::string, int> mStats;
	std::map<std::string, int> mModifier;
	std::map<std::string, int> mStatsCap;
	std::map<std::string, Item*> mEquipment;
	double mSpeed;
	bool mBlock;
	bool mAttack;
	double mRange;
	bool mMoving;
	double mVision;
};

#endif