#ifndef DEF_INPUT_MANAGER
#define DEF_INPUT_MANAGER

#include <SFML/Graphics.hpp>

class Ground;

class InputManager {

public:
	InputManager(sf::RenderWindow& window, Ground* ground);
	~InputManager();

	//M�thodes
	void event();
	void playerControl();
	//getter


	//setter



	//Attributs
protected:
	Ground* mGround;
	sf::RenderWindow& mWindow;
	sf::Event mEvent;

	

};

#endif


