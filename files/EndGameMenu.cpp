#include "EndGameMenu.h"


EndGameMenu::EndGameMenu(sf::RenderWindow& window, sf::RenderWindow& mainWindow, Ground& ground, std::map<std::string, Item*> equipment, int width, int height) : Menu(window), mMainWindow{ mainWindow }, mGround{ ground}, mEquipment { equipment }, mWidth {width}, mHeight {height}
{
	if (!mImage.loadFromFile("../images/parchment.png"))
	{
		// error...
	}

	mBackground.setSize(sf::Vector2f(500.0f, 200.0f));
	mBackground.setOrigin(0, 0);
	mBackground.setPosition(0, 0);
	mTexture.loadFromImage(mImage);
	mBackground.setTexture(&mTexture);


	if (!mFont.loadFromFile("../fonts/ringbearer.ttf"))
	{
		// error...
	}

	sf::Color color{ sf::Color::Black };
	sf::Color borderColor{ sf::Color::Black };
	sf::Color buttonColor{ sf::Color::Transparent };
	int fontSize{ 30 };
	int posX{ 20 };
	int posY{ 65 };
	sf::Vector2f size = sf::Vector2f(80.0f, 40.0f);
	sf::Vector2f buttonSize = sf::Vector2f(150.0f, 40.0f);

	makeTextZone(mEndMessage, fontSize, "Game over ...", posX, posY, 8, 1, color);
	mButtonQuit = makeButton(mButtonQuit, "Quit", size, posX, posY, 8, 2, buttonColor, borderColor, fontSize, color);
	mButtonTryAgain = makeButton(mButtonTryAgain, " Try again ", buttonSize, posX, posY, 13, 2, buttonColor, borderColor, fontSize, color);
	

}

//M�thodes
short int EndGameMenu::event() {
	while (mWindow.isOpen())
	{
		sf::Event event;
		while (mWindow.pollEvent(event))
		{


			if (event.type == sf::Event::MouseButtonPressed)
			{

				if (event.mouseButton.button == sf::Mouse::Left)
				{

					mMousePosition = sf::Mouse::getPosition(mWindow);
					if (mButtonQuit->isClick(mMousePosition))
					{
						mMainWindow.close();
						mWindow.close();
					}

					if (mButtonTryAgain->isClick(mMousePosition))
					{
						mGround.freeProjectiles();
						mGround.freeMonters();
						mGround.freeEntities();
						mGround.freeFountains();
						mGround.freeObjects();
						mGround.freeWalls();
						mGround.spawnPlayer(mEquipment);
						mGround.spawnMonsters(mEquipment);
						mGround.spawnMonsterGenerator();
						mGround.spawnFountains();
						mGround.spawnWallDelimiter();
						mWindow.close();

					}

					
				}


			}

		}




		draw();
	}

	return 0;
}



void EndGameMenu::draw() {
	//Nettoie la surface de dessin
	mWindow.clear();

	//Affiche le background
	mWindow.draw(mBackground);

	//Affiche le text
	mWindow.draw(mEndMessage);
	

	//Affiche les boutons
	mWindow.draw(mButtonQuit->getBackground());
	mWindow.draw(mButtonQuit->getText());

	mWindow.draw(mButtonTryAgain->getBackground());
	mWindow.draw(mButtonTryAgain->getText());
	

	//Affiche le tout
	mWindow.display();
}





//getter



//setter


