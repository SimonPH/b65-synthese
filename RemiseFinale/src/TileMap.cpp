#include "TileMap.h"


TileMap::TileMap()
{
	
}


bool TileMap::load(const std::string& tileset, sf::Vector2u tileSize,  int* tiles, int width, int height)
{
	
	// on charge la texture du tileset
	if (!mTileTexture.loadFromFile(tileset))
		return false;

	// on redimensionne le tableau de vertex pour qu'il puisse contenir tout le niveau
	mVertexArray.setPrimitiveType(sf::Quads);
	mVertexArray.resize(width * height * 4);

	// on remplit le tableau de vertex, avec un quad par tuile
	for (int i{ 0 }; i < width; ++i)
		for (int j{ 0 }; j < height; ++j)
		{
			// on r�cup�re le num�ro de tuile courant
			int tileNumber{ tiles[i + j * width] };

			// on en d�duit sa position dans la texture du tileset
			int tu = tileNumber % (mTileTexture.getSize().x / tileSize.x);
			int tv = tileNumber / (mTileTexture.getSize().x / tileSize.x);

			// on r�cup�re un pointeur vers le quad � d�finir dans le tableau de vertex
			sf::Vertex* quad = &mVertexArray[(i + j * width) * 4];

			// on d�finit ses quatre coins
			quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
			quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
			quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
			quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

			// on d�finit ses quatre coordonn�es de texture
			quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
			quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
			quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
			quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
		}

	return true;
}

void TileMap::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	// on applique la transformation
	states.transform *= getTransform();

	// on applique la texture du tileset
	states.texture = &mTileTexture;

	// et on dessine enfin le tableau de vertex
	target.draw(mVertexArray, states);
}


