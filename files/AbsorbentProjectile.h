#ifndef DEF_ABSORBENT_PROJECTILE
#define DEF_ABSORBENT_PROJECTILE


#include "Projectile.h"
#include "DynamicEntity.h"


class AbsorbentProjectile : public Projectile {

	//M�thodes
public:
	AbsorbentProjectile(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize, sf::Texture* texture,
		DynamicEntity  *master, DynamicEntity  *target, double speed, Ground* ground);
	virtual ~AbsorbentProjectile();
	void action() override;
	void goToTarget() override;
	void stealStaminaTarget();



	//getter


	//Attributs
protected:




};

#endif


