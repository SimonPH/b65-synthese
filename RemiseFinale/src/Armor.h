#ifndef DEF_ARMOR
#define DEF_ARMOR

#include "Item.h"

class Armor : public Item{

	//M�thodes
public:

	Armor(int level, std::string type, std::string itemName, int durability, int defense);
	Armor();
	virtual ~Armor();


	//getter
	int getDefense() const;




	//Attributs
protected:
	int mDefense;

};

#endif