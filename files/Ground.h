#ifndef DEF_GROUND
#define DEF_GROUND

#define BASE_HITPOINT 100
#define BASE_STAMINA 100
#define BASE_BONUS 0.05


#include <list>

#include "Monster.h"
#include "RangeMonster.h"
#include "Player.h"
#include "Projectile.h"
#include "Object.h"
#include "Fountain.h"
#include "SoundManager.h"





class Ground {

	//M�thodes
public:

	Ground(int width, int height, sf::RenderWindow& window, SoundManager& soundManager, std::map<std::string, Item*> & equipment);
	~Ground();
	void addTexture(std::string, sf::Texture*);
	void spawnPlayer(std::map<std::string, Item*> equipment);
	void spawnMonsters(std::map<std::string, Item*> equipment);
	void spawnMonsterGenerator();
	void spawnFountains();
	bool sameEntity(const Entity* me,const Entity* entity);
	bool checkCollision(const Entity* me, int tempPosX, int tempPosY);
	bool checkSpawnCollision(int tempPosX, int tempPosY);
	void spawnWallDelimiter();
	void freeEntities();
	void freeObjects();
	void freeFountains();
	void freeWalls();
	void obtainTarget();
	void freeMonters();
	void freeProjectiles();
	void relinkEntities();
	void manageProjectiles(const sf::Time& deltaTime);
	void refillPlayer();
	void refillFountain();
	std::vector<std::map<std::string, int>> countingStatsPoints();
	void monstersTick(const sf::Time& deltaTime);
	bool monstersIsAlive();
	void draw();
	void drawMonsters();
	void drawObjects();
	void drawWalls();
	void drawProjectiles();


	//getter
	Player* & getPlayer();
	std::list<Monster*> & getMonsters();
	std::vector<RangeMonster> & getRangeMonsters();
	std::vector<Object*> & getObjects();
	std::vector<Entity> & getWalls();
	std::vector<Fountain*> & getFountains();
	std::list<Entity*> & getEntities();
	std::map<std::string, Item*> & getEquipment();
	std::vector<Projectile*> & getProjectiles();
	sf::Texture* getMonstersTexture(std::string) const;
	int getWidth() const;
	int getHeight() const;
	SoundManager & getSoundManager();




	//setter








	//Attributs
protected:
	Player *mPlayer;
	std::list<Monster*> mMonsters;
	std::vector<RangeMonster> mRangeMonsters;
	std::vector<Object*> mObjects;
	std::vector<Fountain*> mFountains;
	std::vector<Entity> mWalls;
	std::list<Entity*> mEntities;
	std::map<std::string, Item*> & mEquipment;
	std::map<std::string, sf::Texture*> mMonstersTexture;
	std::vector<Projectile*> mProjectiles;
	sf::RenderWindow& mWindow;
	SoundManager& mSoundManager;
	int mWidth;
	int mHeight;

};



#endif
