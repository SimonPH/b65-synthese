#ifndef DEF_FOUNTAIN
#define DEF_FOUNTAIN

#include <SFML/Graphics.hpp>

#include "Object.h"



class Fountain : public Object {

	//M�thodes
public:

	Fountain(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize,
		sf::Texture* texture, Ground* ground, bool life);
	Fountain();
	virtual ~Fountain();
	void action();
	void refill();

	//getter


	//setter



	//Attributs
protected:
	bool mFull;
	bool mLife;



};

#endif
