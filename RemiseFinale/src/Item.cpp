#include "Item.h"

Item::Item (int level, std::string type, std::string itemName, int durability) : 
	mLevel{ level }, mType{ type }, mItemName{ itemName }, mDurability{ durability }
{

}

Item::Item() {

}

Item::~Item(){

}

//M�thodes


//getter

int Item::getLevel() const {
	return mLevel;
}

std::string Item::getType() const {
	return mType;
}

std::string Item::getItemName() const {
	return mItemName;
}

int Item::getDurability() const {
	return mDurability;
}

//setter
void Item::setDurability(int amount) {
	mDurability = amount;
}

