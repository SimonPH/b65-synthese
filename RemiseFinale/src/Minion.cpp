#include "Minion.h" 

#include <SFML/Graphics.hpp>

Minion::Minion(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats,
	std::map<std::string, int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed,
	double posX, double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target,
	sf::Texture *texture, Ground* ground,  DynamicEntity* master, double posXToMaster, double posYToMaster)
	: Monster(level, name, hitPoint, stamina, stats, statsCap, modifier, equipment, speed, posX, posY, sizeX, sizeY, spriteSize,
		range, vision, target, texture, ground), mMaster{ master }, mPosXToMaster{ posXToMaster }, mPosYToMaster { posYToMaster }
{

	mSprite.setTexture(*mTexture);

	mRect.left = 0;
	mRect.top = spriteSize * 10;
	mRect.height = spriteSize;
	mRect.width = spriteSize;

	mSprite.setTextureRect(mRect);


}

Minion::~Minion() {


}


bool Minion::inVision() {


	double distance{ ((mPosX - mTarget->getPosX())*(mPosX - mTarget->getPosX())) + ((mPosY - mTarget->getPosY())*(mPosY - mTarget->getPosY())) };
	distance = sqrt(distance);
	if (distance <= mVision) {
		return true;
	}
	moveToMaster();
	

	return false;


}

void Minion::moveToMaster() {

	double tempPosX{ mPosX };
	double tempPosY{ mPosY };

	if (!inRange()) {
		mChase = true;

		if (abs(mPosX - mPosXToMaster) > 5 && abs(mPosY - mPosYToMaster) > 5) {

			if (mPosX < mPosXToMaster)
			{
				tempPosX += mSpeed;
				setRectRow(11);

				if (mPosY < mPosYToMaster)
				{
					tempPosY += mSpeed;
					//setRectRow(10);
				}
				else {
					tempPosY -= mSpeed;
					//setRectRow(8);
				}
			}
			else {
				tempPosX -= mSpeed;
				setRectRow(9);

				if (mPosY < mPosYToMaster)
				{
					tempPosY += mSpeed;
					//setRectRow(10);
				}
				else {
					tempPosX -= mSpeed;
					//setRectRow(8);
				}
			}

			
			mPosX = tempPosX;
			mPosY = tempPosY;



			mSprite.setPosition(mPosX, mPosY);
			spriteChanges(8);
		}
		else {
			//setRectCol(0);
		}
	}



}

//setter

void Minion::setPosToMaster(double x, double y) {
	mPosXToMaster = x;
	mPosYToMaster = y;
}

//getter

DynamicEntity* Minion::getMaster() const {
	return mMaster;
}


