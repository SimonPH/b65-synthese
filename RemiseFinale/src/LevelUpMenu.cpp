#include "LevelUpMenu.h"


LevelUpMenu::LevelUpMenu(sf::RenderWindow& window, Player& player) : Menu(window), mPlayer {player}
{
	if (!mImage.loadFromFile("../images/parchment.png"))
	{
		// error...
	}

	mBackground.setSize(sf::Vector2f(500.0f, 500.0f));
	mBackground.setOrigin(0, 0);
	mBackground.setPosition(0, 0);
	mTexture.loadFromImage(mImage);
	mBackground.setTexture(&mTexture);


	if (!mFont.loadFromFile("../fonts/ringbearer.ttf"))
	{
		// error...
	}

	sf::Color color{ sf::Color::Black };
	sf::Color borderColor{ sf::Color::Black };
	sf::Color buttonColor{ sf::Color::Transparent };
	int fontSize{ 30 };
	int posX{ 20 };
	int posY{ 65 };
	std::map<std::string, int> stats = mPlayer.getStats();
	sf::Vector2f size = sf::Vector2f(150.0f, 40.0f);
	sf::Vector2f buttonSize = sf::Vector2f(40.0f, 40.0f);

	makeTextZone(mStatsPointsText, fontSize, "Points to spend :", posX, posY, 1, 1, color);
	makeTextZone(mVitalityText, fontSize, "Vitality :", posX, posY, 1, 2, color);
	makeTextZone(mStrengthText, fontSize, "Strength :", posX, posY, 1, 3, color);
	makeTextZone(mAgilityText, fontSize, "Agility :", posX, posY, 1, 4, color);
	makeTextZone(mIntelligenceText, fontSize, "Intelligence :", posX, posY, 1, 5, color);
	makeTextZone(mEnduranceText, fontSize, "Endurance :", posX, posY, 1, 6, color);

	makeTextZone(mStatsPointsTextValue, fontSize, std::to_string(stats.size()), posX, posY, 16, 1, color);
	makeTextZone(mVitalityTextValue, fontSize, std::to_string(stats["Vitality"]), posX, posY, 16, 2, color);
	makeTextZone(mStrengthTextValue, fontSize, std::to_string(stats["Strength"]), posX, posY, 16, 3, color);
	makeTextZone(mAgilityTextValue, fontSize, std::to_string(stats["Agility"]), posX, posY, 16, 4, color);
	makeTextZone(mIntelligenceTextValue, fontSize, std::to_string(stats["Intelligence"]), posX, posY, 16, 5, color);
	makeTextZone(mEnduranceTextValue, fontSize, std::to_string(stats["Endurance"]), posX, posY, 16, 6, color);
	
	mButtonConfirm = makeButton(mButtonConfirm,"Confirm",size, posX, posY, 10 , 7, buttonColor,borderColor,fontSize,color);

	mButtonLessVitality = makeButton(mButtonLessVitality, " - ", buttonSize, posX, posY, 13, 2, buttonColor, borderColor, fontSize, color);
	mButtonLessStrength = makeButton(mButtonLessStrength, " - ", buttonSize, posX, posY, 13, 3, buttonColor, borderColor, fontSize, color);
	mButtonLessAgility = makeButton(mButtonLessAgility, " - ", buttonSize, posX, posY, 13, 4, buttonColor, borderColor, fontSize, color);
	mButtonLessIntelligence = makeButton(mButtonLessIntelligence, " - ", buttonSize, posX, posY, 13, 5, buttonColor, borderColor, fontSize, color);
	mButtonLessEndurance = makeButton(mButtonLessEndurance, " - ", buttonSize, posX, posY, 13, 6, buttonColor, borderColor, fontSize, color);

	mButtonMoreVitality = makeButton(mButtonMoreVitality, " + ", buttonSize, posX, posY, 19, 2, buttonColor, borderColor, fontSize, color);
	mButtonMoreStrength = makeButton(mButtonMoreStrength, " + ", buttonSize, posX, posY, 19, 3, buttonColor, borderColor, fontSize, color);
	mButtonMoreAgility = makeButton(mButtonMoreAgility, " + ", buttonSize, posX, posY, 19, 4, buttonColor, borderColor, fontSize, color);
	mButtonMoreIntelligence = makeButton(mButtonMoreIntelligence, " + ", buttonSize, posX, posY, 19, 5, buttonColor, borderColor, fontSize, color);
	mButtonMoreEndurance = makeButton(mButtonMoreEndurance, " + ", buttonSize, posX, posY, 19, 6, buttonColor, borderColor, fontSize, color);

}

//M�thodes
short int LevelUpMenu::event() {
	while (mWindow.isOpen())
	{
		sf::Event event;
		while (mWindow.pollEvent(event))
		{
			

			if (event.type == sf::Event::MouseButtonPressed)
			{

				if (event.mouseButton.button == sf::Mouse::Left)
				{

					mMousePosition = sf::Mouse::getPosition(mWindow);
					if (mButtonConfirm->isClick(mMousePosition))
					{
						if (isValid()) {
							setStats();
							mWindow.close();
						}
					}

					lessStats(mButtonLessVitality, mMousePosition, mVitalityTextValue, "Vitality");
					moreStats(mButtonMoreVitality, mMousePosition, mVitalityTextValue);

					lessStats(mButtonLessStrength, mMousePosition, mStrengthTextValue, "Strength");
					moreStats(mButtonMoreStrength, mMousePosition, mStrengthTextValue);

					lessStats(mButtonLessAgility, mMousePosition, mAgilityTextValue, "Agility");
					moreStats(mButtonMoreAgility, mMousePosition, mAgilityTextValue);

					lessStats(mButtonLessIntelligence, mMousePosition, mIntelligenceTextValue, "Intelligence");
					moreStats(mButtonMoreIntelligence, mMousePosition, mIntelligenceTextValue);

					lessStats(mButtonLessEndurance, mMousePosition, mEnduranceTextValue, "Endurance");
					moreStats(mButtonMoreEndurance, mMousePosition, mEnduranceTextValue);
				}


			}

		}

		


		draw();
	}

	return 0;
}

void LevelUpMenu::lessStats(Button* button, sf::Vector2i mousePosition, sf::Text& text, std::string stat) {
	if (button->isClick(mousePosition))
	{
		int statNumber = std::stoi(std::string(text.getString()));
		int pointNumber = std::stoi(std::string(mStatsPointsTextValue.getString()));

		if (statNumber - 1 >= mPlayer.getStats()[stat] ) {
			statNumber -= 1;
			text.setString(std::to_string(statNumber));
			pointNumber += 1;
			mStatsPointsTextValue.setString(std::to_string(pointNumber));

		}
		
	}
}

void LevelUpMenu::moreStats(Button* button, sf::Vector2i mousePosition, sf::Text& text) {
	if (button->isClick(mousePosition))
	{
		int statNumber = std::stoi(std::string(text.getString()));
		int pointNumber = std::stoi(std::string(mStatsPointsTextValue.getString()));

		if (pointNumber > 0) {
			pointNumber -= 1;
			mStatsPointsTextValue.setString(std::to_string(pointNumber));

			statNumber += 1;
			text.setString(std::to_string(statNumber));

		}

	}
}

void LevelUpMenu::draw() {
	//Nettoie la surface de dessin
	mWindow.clear();

	//Affiche le background
	mWindow.draw(mBackground);

	//Affiche le text
	mWindow.draw(mStatsPointsText);
	mWindow.draw(mVitalityText);
	mWindow.draw(mStrengthText);
	mWindow.draw(mAgilityText);
	mWindow.draw(mIntelligenceText);
	mWindow.draw(mEnduranceText);
	mWindow.draw(mStatsPointsTextValue);
	mWindow.draw(mVitalityTextValue);
	mWindow.draw(mStrengthTextValue);
	mWindow.draw(mAgilityTextValue);
	mWindow.draw(mIntelligenceTextValue);
	mWindow.draw(mEnduranceTextValue);

	//Affiche les boutons
	mWindow.draw(mButtonConfirm->getBackground());
	mWindow.draw(mButtonConfirm->getText());

	mWindow.draw(mButtonLessVitality->getBackground());
	mWindow.draw(mButtonLessVitality->getText());
	mWindow.draw(mButtonLessStrength->getBackground());
	mWindow.draw(mButtonLessStrength->getText());
	mWindow.draw(mButtonLessAgility->getBackground());
	mWindow.draw(mButtonLessAgility->getText());
	mWindow.draw(mButtonLessIntelligence->getBackground());
	mWindow.draw(mButtonLessIntelligence->getText());
	mWindow.draw(mButtonLessEndurance->getBackground());
	mWindow.draw(mButtonLessEndurance->getText());

	mWindow.draw(mButtonMoreVitality->getBackground());
	mWindow.draw(mButtonMoreVitality->getText());
	mWindow.draw(mButtonMoreStrength->getBackground());
	mWindow.draw(mButtonMoreStrength->getText());
	mWindow.draw(mButtonMoreAgility->getBackground());
	mWindow.draw(mButtonMoreAgility->getText());
	mWindow.draw(mButtonMoreIntelligence->getBackground());
	mWindow.draw(mButtonMoreIntelligence->getText());
	mWindow.draw(mButtonMoreEndurance->getBackground());
	mWindow.draw(mButtonMoreEndurance->getText());

	//Affiche le tout
	mWindow.display();
}

void LevelUpMenu::setStats() {
	std::map<std::string, int> stats = { { "Vitality" ,0 },{ "Strength" ,0 },{ "Agility" ,0 },{ "Intelligence" ,0 },{ "Endurance" ,0 } };
	int vitality{ std::stoi(std::string(mVitalityTextValue.getString())) };
	int strength{ std::stoi(std::string(mStrengthTextValue.getString())) };
	int agility{ std::stoi(std::string(mAgilityTextValue.getString())) };
	int intelligence{ std::stoi(std::string(mIntelligenceTextValue.getString())) };
	int endurance{ std::stoi(std::string(mEnduranceTextValue.getString())) };
	stats["Vitality"] = vitality;
	stats["Strength"] = strength;
	stats["Agility"] = agility;
	stats["Intelligence"] = intelligence;
	stats["Endurance"] = endurance;
	mPlayer.setStats(stats);

}

bool LevelUpMenu::isValid() {
	int pointNumber = std::stoi(std::string(mStatsPointsTextValue.getString()));

	if (pointNumber == 0) {
		return true;
	}
	else {
		return false;
	}

}



//getter



//setter


