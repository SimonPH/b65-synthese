#ifndef DEF_INVOKER_MONSTER
#define DEF_INVOKER_MONSTER


#include "Monster.h"
#include "Minion.h"
#include "Projectile.h"

class Ground;

class InvokerMonster : public Monster {

	//M�thodes
public:
	InvokerMonster(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats, std::map<std::string,
		int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed, double posX,
		double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target, sf::Texture *texture,
		Ground* ground, sf::Texture *minionTexture);
	virtual ~InvokerMonster();
	void attack() override;
	int determineLevel();
	int checkMinionsIsAlive() const;
	void moveToTarget() override;
	bool isAlive() override;
	void killMinions();
	void minionsPostition();



	//getter
	


	//Attributs
protected:
	sf::Texture *mMinionTexture;
	std::list<Minion*> mMinions;

};

#endif

