#ifndef DEF_OBJECT
#define DEF_OBJECT

#include "Entity.h"

#include <SFML/Graphics.hpp>


class Ground;


class Object : public Entity {

	//M�thodes
public:

	Object(std::string name, double posX, double posY, double sizeX, double sizeY, int spriteSize, sf::Texture *texture, Ground* ground);
	Object();
	virtual ~Object();
	virtual void action();



	//getter


	//setter


	//Attributs

protected:
	
};

#endif

