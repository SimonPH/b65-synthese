#ifndef DEF_SHIELD
#define DEF_SHIELD

#include "Item.h"

class Shield : public Item{

	//M�thodes
public:

	Shield(int level, std::string type, std::string itemName, int durability, int defense);
	virtual ~Shield();

	//getter
	int getDefense() const;




	//Attributs
protected:
	int mDefense;

};

#endif