#include "MonsterGenerator.h"
#include "Ground.h"


MonsterGenerator::MonsterGenerator(std::string name, double posX, double posY, double sizeX, double sizeY,  int spriteSize,
	sf::Texture* texture, Ground* ground) : Object(name, posX, posY, sizeX, sizeY, spriteSize, texture, ground)
{
	mSprite.setTexture(*mTexture);

}

MonsterGenerator::MonsterGenerator() {


}

MonsterGenerator::~MonsterGenerator() {


}

//M�thodes
void MonsterGenerator::action() {

	if (!mGround->monstersIsAlive()) {

		mGround->getSoundManager().getSound("generateMonster").play();
		mGround->freeMonters();
		mGround->freeEntities();
		mGround->relinkEntities();
		mGround->refillPlayer();
		mGround->refillFountain();
		mGround->spawnMonsters(mGround->getEquipment());

	}
}


//getter


//setter


