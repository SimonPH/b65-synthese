#ifndef DEF_BUTTON
#define DEF_BUTTON

#include <SFML/Graphics.hpp>


class Button
{
	public:

		Button(std::string text, sf::Vector2f size, int posX, int posY);
		~Button() = default;
		sf::RectangleShape Button::getBackground();
		sf::Text Button::getText();
		void Button::setBackgroundColor(sf::Color color, sf::Color borderColor);
		void Button::setFont(sf::Font& font, int size, sf::Color color);
		bool Button::isClick(sf::Vector2i mousePosition);


	private:

		sf::Text mText;
		sf::RectangleShape mBackground;
};

#endif

