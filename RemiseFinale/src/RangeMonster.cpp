#include "RangeMonster.h" 
#include "Missile.h"
#include "Ground.h"

#include <SFML/Graphics.hpp>

RangeMonster::RangeMonster(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats,
	std::map<std::string, int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed,
	double posX, double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target,
	sf::Texture *texture, Ground* ground,  sf::Texture *projectileTexture,std::vector<Projectile*> & projectiles)
	: Monster(level, name, hitPoint, stamina, stats, statsCap, modifier, equipment, speed, posX, posY, sizeX, sizeY, spriteSize,
		range, vision, target, texture , ground),
	mProjectileTexture{ projectileTexture }, mProjectiles{ projectiles}
{

	mSprite.setTexture(*mTexture);

	mRect.left = 0;
	mRect.top = spriteSize * 10;
	mRect.height = spriteSize;
	mRect.width = spriteSize;

	mSprite.setTextureRect(mRect);


}

RangeMonster::~RangeMonster() {


}


void RangeMonster::attack() {

	if (enoughEnergy(ATTACK_COST)) {
		mGround->getSoundManager().getSound("fireball").stop();
		mGround->getSoundManager().getSound("fireball").play();
		mStamina -= ATTACK_COST;
		setMoving(false);
		setAttacking(true);
		setBlocking(false);

		mProjectiles.push_back(new Missile("Fireball", mPosX, mPosY, 10, 10, 64, mProjectileTexture, this, mTarget, 10.0, mGround));

	}

}

//getter
std::vector<Projectile*> & RangeMonster::getProjectiles() {

	return mProjectiles;
}

