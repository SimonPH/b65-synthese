#include "InvokerMonster.h" 
#include "Ground.h"

#include <SFML/Graphics.hpp>


InvokerMonster::InvokerMonster(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats,
	std::map<std::string, int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment, double speed,
	double posX, double posY, double sizeX, double sizeY, int spriteSize, double range, double vision, DynamicEntity *target,
	sf::Texture *texture, Ground* ground,  sf::Texture *minionTexture)
	: Monster(level, name, hitPoint, stamina, stats, statsCap, modifier, equipment, speed, posX, posY, sizeX, sizeY, spriteSize, range,
		vision, target, texture, ground),mMinionTexture{ minionTexture }, mMinions{ }
{

	mSprite.setTexture(*mTexture);

	mRect.left = 0;
	mRect.top = spriteSize * 10;
	mRect.height = spriteSize;
	mRect.width = spriteSize;

	mSprite.setTextureRect(mRect);


}

InvokerMonster::~InvokerMonster() {


}



void InvokerMonster::attack() {

	int maxMinionsAmount{ 4 };
	int minionsAmount{ checkMinionsIsAlive() };
	int newMinionsAmount{ maxMinionsAmount - minionsAmount };
	double posX{ 0 }, posY{ 0 };

		if (newMinionsAmount > 0) {
			if (enoughEnergy(ATTACK_COST)) {
				mGround->getSoundManager().getSound("invocation").stop();
				mGround->getSoundManager().getSound("invocation").play();
				mStamina -= ATTACK_COST;
				setMoving(false);
				setAttacking(true);
				setBlocking(false);

				int level{ determineLevel() };
				int radius{ 100 };
				int angle{ 45 };
				int angleSup{90};

				for (int i{ maxMinionsAmount - newMinionsAmount }; i < maxMinionsAmount; i++) {
					
					posX = this->getPosX() + radius * cos(angle);
					posY = this->getPosY() + radius * sin(angle);

					if (mGround->checkSpawnCollision(posX, posY)) {

						mMinions.push_back(new Minion(level, "Minion", this->getStatsCap()["Vitality"], this->getStatsCap()["Endurance"],
							this->getStats(), this->getStatsCap(), this->getModifier(), this->getEquipment(), 10.0, posX, posY,
							this->getSizeX(), this->getSizeY(), this->getSpriteSize(), 80.0, 170.0, this->getTarget(), mMinionTexture, mGround, this, posX, posY));

						mGround->getMonsters().push_back(mMinions.back());
						mGround->getEntities().push_back(mMinions.back());

						angle += angleSup;


					}



				}

			}
		}

}

int InvokerMonster::determineLevel() {

	int level{ (int)floor(mLevel / 2) };
	if (level >= 1) {
		return level;
	}
	else {
		level = 1;
		return level;
	}


}

int InvokerMonster::checkMinionsIsAlive() const {

	int minionsAmount{ 0 };
	for (auto &m : mMinions) {
		if (m->isAlive()) {
			minionsAmount += 1;
		}
	}

	return minionsAmount;
}

void InvokerMonster::minionsPostition() {

	double posX{ 0 }, posY{ 0 };
	int radius{ 100 };
	int angle{ 0 };
	int angleSup{ 90 };

	for (auto &m : mMinions) {
		
		posX = m->getMaster()->getPosX() + radius * cos(angle);
		posY = m->getMaster()->getPosY() + radius * sin(angle);
		m->setPosToMaster(posX, posY);

		angle += angleSup;


	}

}

void InvokerMonster::moveToTarget() {

	double tempPosX{ mPosX };
	double tempPosY{ mPosY };

	if (!inRange()) {
		mChase = true;


		if (mPosX < mTarget->getPosX())
		{
			tempPosX += mSpeed;
			setRectRow(11);

			if (mPosY < mTarget->getPosY())
			{
				tempPosY += mSpeed;
				//setRectRow(10);
			}
			else {
				tempPosY -= mSpeed;
				//setRectRow(8);
			}
		}
		else {
			tempPosX -= mSpeed;
			setRectRow(9);

			if (mPosY < mTarget->getPosY())
			{
				tempPosY += mSpeed;
				//setRectRow(10);
			}
			else {
				tempPosX -= mSpeed;
				//setRectRow(8);
			}
		}

		if (mGround->checkCollision(this , tempPosX, tempPosY)) {
			if (inLimitX(tempPosX)) {
				mPosX = tempPosX;
				minionsPostition();

			}


			if (inLimitY(tempPosY)) {
				mPosY = tempPosY;
				minionsPostition();
			}


		}


		mSprite.setPosition(mPosX, mPosY);
		spriteChanges(8);
	}
	else {
		//setRectCol(0);
	}



}

bool InvokerMonster::isAlive() {
	if (mHitPoint > 0) {
		return true;

	}
	else {
		killMinions();
		return false;
	}
}

void InvokerMonster::killMinions() {

	for (auto &m : mMinions) {
		m->setHitPoint(0);
	}
}



//getter


