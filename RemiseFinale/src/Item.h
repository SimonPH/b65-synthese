#ifndef DEF_ITEM
#define DEF_ITEM

#include <string>


class Item {

	//M�thodes
public:

	Item(int level, std::string type, std::string itemName, int durability);
	Item();
	virtual ~Item();

	//getter
	int getLevel() const;
	std::string getType() const;
	std::string getItemName() const;
	int getDurability() const;

	//setter
	void setDurability(int amount);


	//Attributs
protected:
	int mLevel;
	std::string mType;
	std::string mItemName;
	int mDurability;
	
};

#endif