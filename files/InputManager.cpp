#include "InputManager.h"
#include "Ground.h"

#include <SFML/Window/Keyboard.hpp> // Donne acc�s au clavier en temps r�el



InputManager::InputManager(sf::RenderWindow& window, Ground* ground) : mWindow{ window }, mGround{ ground }, mEvent {}
{
	

}

InputManager::~InputManager() {

}


//M�thodes
void InputManager::event() {

	while (mWindow.pollEvent(mEvent))
	{
		if (mEvent.type == sf::Event::Closed) {
			mWindow.close();
		}



		//Detection de touche pour courrir
		if (mEvent.type == sf::Event::KeyPressed) {
			if (mEvent.key.code == sf::Keyboard::LShift) {
				mGround->getPlayer()->run();
			}

			if (mEvent.key.code == sf::Keyboard::E) {

				mGround->getPlayer()->inRangeObject();




			}
		}
		else {
			mGround->getPlayer()->setMoving(false);
			//player.setRectCol(0); // DANGER !!! ANIMATION
		}

		//Detection de touche pour arr�ter de courrir
		if (mEvent.type == sf::Event::KeyReleased) {
			if (mEvent.key.code == sf::Keyboard::LShift) {
				mGround->getPlayer()->walk();
			}
		}

		if (mEvent.type == sf::Event::MouseButtonPressed)
		{
			//Detection de touche pour l'attaque
			if (mEvent.mouseButton.button == sf::Mouse::Left)
			{



				if (!mGround->getPlayer()->getTarget()) {
					mGround->obtainTarget();
					if (mGround->getPlayer()->getTarget()) {
						if (mGround->getPlayer()->inRange()) {
							mGround->getPlayer()->attack();
						}
					}
				}
				else {
					if (mGround->getPlayer()->inRange()) {
						mGround->getPlayer()->attack();
					}
					else {
						mGround->obtainTarget();

					}

				}


			}

			//Dectection de touche pour le blocage
			if (mEvent.mouseButton.button == sf::Mouse::Right)
			{
				mGround->getPlayer()->setBlocking(true);
				mGround->getPlayer()->setAttacking(false);


			}
		}
	}

	//Detecte lorsque le joueur ne bloque plus
	if (mEvent.type == sf::Event::MouseButtonReleased)
	{
		if (mEvent.mouseButton.button == sf::Mouse::Right)
		{
			mGround->getPlayer()->setBlocking(false);
		}


	}

}

void InputManager::playerControl() {

	//Dectection de touche pour le deplacement du joueur
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		mGround->getPlayer()->move('U');
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		mGround->getPlayer()->move('D');
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		mGround->getPlayer()->move('L');
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		mGround->getPlayer()->move('R');

	}

}



//getter



//setter


