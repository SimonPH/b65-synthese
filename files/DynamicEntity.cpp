#include "DynamicEntity.h"
#include "Ground.h"
#include "Armor.h"
#include "Weapon.h"
#include "Shield.h"

#include <stdlib.h>  // abs


DynamicEntity::DynamicEntity(int level, std::string name,int hitPoint, int stamina, std::map<std::string, int> stats,
	std::map<std::string, int> statsCap, std::map<std::string, int> modifier, std::map<std::string,Item*> equipment,
	double speed, double posX, double posY, double sizeX, double sizeY, int spriteSize, double range, double vision,
	DynamicEntity *target, sf::Texture* texture, Ground* ground)
	: Entity(name,  posX,  posY,  sizeX, sizeY, spriteSize, texture, ground), mLevel{ level }, mHitPoint{ hitPoint }, mStamina{ stamina }, mStats{ stats }, mStatsCap{statsCap}, mModifier {
	modifier}, mEquipment{ equipment },mSpeed{ speed }, mAttack{ false }, mBlock{ false }, mRange{ range }, mVision{ vision },
	mMoving{ false }, mTarget{ target }
	

{

}

DynamicEntity::DynamicEntity() {

}

DynamicEntity::~DynamicEntity() {

}


	
//M�thodes

void DynamicEntity::attack(){
	
	if (enoughEnergy(ATTACK_COST)) {
		mGround->getSoundManager().getSound("melee").stop();
		mGround->getSoundManager().getSound("melee").play();
		mStamina -= ATTACK_COST;
		setMoving(false);
		setAttacking(true);
		setBlocking(false);
		int damage = getDamage() + mModifier["Strength"];

		if (mTarget->mBlock)
		{
			mTarget->blockAttack(damage);
		}
		else {
			mTarget->armorBlock(damage);

		}


	}

	
}

//verifie si on peu bloquer
void DynamicEntity::blockAttack(int takenDamages) {

	if(enoughEnergy(BLOCK_COST)){
		takenDamages -= getBlockValue();
		mStamina -= BLOCK_COST;

	}
	if (takenDamages > 0) {
		armorBlock(takenDamages);
	}

}

// reduction de degat avec l'armure
void DynamicEntity::armorBlock(int takenDamages) {
	int armorDefense = getArmorDefense();
	if (armorDefense - takenDamages < 0) {
		int takenDamagesOnHp{ abs(armorDefense - takenDamages) };
		receiveDamage(takenDamagesOnHp);
	}

}


void DynamicEntity::receiveDamage(int takenDamages) {
	
	mHitPoint -= takenDamages;
	if (mHitPoint < 0) {
		mHitPoint = 0;
	}
}

void DynamicEntity::hitPointRegeneration(const sf::Time& deltaTime) {
	static double accumulatedTimeRegen{ 0 };
	accumulatedTimeRegen += deltaTime.asSeconds();

	if (accumulatedTimeRegen > HITPOINT_REGEN_DELAY) {

		
		if (mHitPoint + HITPOINT_REGEN + mModifier["Vitality"] > mStatsCap["Vitality"]) {
			mHitPoint = mStatsCap["Vitality"];
		}
		else {
			mHitPoint += HITPOINT_REGEN + mModifier["Vitality"];
		}

		accumulatedTimeRegen = 0;
	}
}

void DynamicEntity::staminaRegeneration(const sf::Time& deltaTime) {
	static double accumulatedTimeRegen{ 0 };
	accumulatedTimeRegen += deltaTime.asSeconds();

	if (accumulatedTimeRegen > STAMINA_REGEN_DELAY) {

		if (mStamina + STAMINA_REGEN + mModifier["Endurance"] > mStatsCap["Endurance"]) {
			mStamina = mStatsCap["Endurance"];
		}
		else {
			mStamina += STAMINA_REGEN + mModifier["Endurance"];
		}

		accumulatedTimeRegen = 0;
	}
}

//applique les modificateurs bonus des attibuts comme la force , etc
void DynamicEntity::setStatsModifier() {
	for (auto const& stat : mStats)
	{
		if (stat.second > 10) {
			mModifier[stat.first] = floor((stat.second - 10) / 2);
		}

	}
}

void DynamicEntity::setStatsCap() {
	mStatsCap = { { "Vitality" ,floor(BASE_HITPOINT * (1 + mModifier["Vitality"] * BASE_BONUS)) },{ "Intelligence" ,0 },{ "Endurance" , floor(BASE_STAMINA * (1 + mModifier["Endurance"]* BASE_BONUS)) } };

}

bool DynamicEntity::isAlive() {
	if (mHitPoint > 0) {
		return true;

	}
	else {
		return false;
	}
}

bool DynamicEntity::inLimitX(double tempPosX) {

	if (tempPosX > 0 - mSizeX && tempPosX < mGround->getWidth() - mSizeX) {
		
		return true;

	}
	return false;


}

bool DynamicEntity::inLimitY(double tempPosY) {

	
	if (tempPosY > 0 - mSizeY && tempPosY < mGround->getHeight() - mSizeY) {
		
		return true;
	}

	return false;
	


}

bool DynamicEntity::inVision() {

	
	double distance{ ((mPosX - mTarget->getPosX())*(mPosX - mTarget->getPosX())) + ((mPosY - mTarget->getPosY())*(mPosY - mTarget->getPosY())) };
	distance = sqrt(distance);
	if (distance <= mVision) {
		return true;
	}


	return false;


}

bool DynamicEntity::inRange() const {

	double distance{ ((mPosX - mTarget->getPosX())*(mPosX - mTarget->getPosX())) + ((mPosY - mTarget->getPosY())*(mPosY - mTarget->getPosY())) };
	distance = sqrt(distance);
	if (distance <= mRange) {
		return true;
	}


	return false;

}

bool DynamicEntity::isMoving() const {
	return mMoving;
}

bool DynamicEntity::isAttacking() const {
	return mAttack;

}

bool DynamicEntity::isBlocking() const {
	return mBlock;

}



bool DynamicEntity::enoughEnergy(int cost) const {
	if (mStamina - cost >= 0) {
		return true;
	}
	else {
		return false;
	}
}

// change les sprites lorsqu'on bouge
void DynamicEntity::spriteChanges(int col, const sf::Time& deltaTime) {

	static double accumulatedTime{ 0 };
	accumulatedTime += deltaTime.asSeconds();
	if (accumulatedTime > SPRITE_CHANGE_DELAY)
	{
		if (!isAttacking()) {
			if (!isBlocking()) {
				if (isMoving()) {
					if (mRect.left == mSpriteSize * col)
						mRect.left = 0;
					else
						mRect.left += mSpriteSize;

					mSprite.setTextureRect(mRect);
				}
			}
		}
		accumulatedTime = 0;
	}

}



void DynamicEntity::attackAnimation(const sf::Time& deltaTime) {
	static double accumulatedTime{ 0 };
	static int counter{ 0 };
	static int row{ 0 };
	accumulatedTime += deltaTime.asSeconds();

	if (accumulatedTime > ATTACK_ANIMATION_DELAY)
	{
		if (isAttacking()) {

			if (counter == 0) {
				row = mRect.top;

				int rowAttack{ 0 };


				if (mRect.top == mSpriteSize * 8) {
					rowAttack = 0;
				}
				else if (mRect.top == mSpriteSize * 9) {
					rowAttack = 190;
				}
				else if (mRect.top == mSpriteSize * 10) {
					rowAttack = 190 * 2;
				}
				else if (mRect.top == mSpriteSize * 11) {
					rowAttack = 190 * 3;
				}

				//Mesures
				//rang�es
				//mRect.top += 190;
				//colonnes
				//mRect.left += 190;

				//Je me place au plus bas dans la spritesheet au niveau des d�placements
				setRectRow(20);
				setRectCol(0);
				//setRectCol(1);

				//Je m'ajuste sur la partie attaque de la spritesheet
				//Au niveau des rang�es
				mRect.top += 130;
				//Au niveau des colonnes
				mRect.left += 64;

				//Ajuste a la bonne range d'attaque
				mRect.top += rowAttack;

				counter += 1;
			}
			else {

				
				if (mRect.left >= 190 * 5 + 64) {
					setAttacking(false);
					counter = 0;
					mRect.top = row;
					mRect.left = 0;
				}
				else {
					mRect.left += 190;
				}
			}

			mSprite.setTextureRect(mRect);
		}
		accumulatedTime = 0;
	}

}

void DynamicEntity::blockAnimation(const sf::Time& deltaTime) {
	static double accumulatedTime{ 0 };
	static int counter{ 0 };
	static int row{ 0 };

	accumulatedTime += deltaTime.asSeconds();

	if (accumulatedTime > BLOCK_ANIMATION_DELAY)
	{
		row = mRect.top;

		if (isBlocking()) {

			if (counter == 0) {

				

				if (mRect.top == mSpriteSize * 8) {
					setRectRow(4);
				}
				else if (mRect.top == mSpriteSize * 9) {
					setRectRow(5);
				}
				else if (mRect.top == mSpriteSize * 10) {
					setRectRow(6);
				}
				else if (mRect.top == mSpriteSize * 11) {
					setRectRow(7);
				}

				counter += 1;
			}
			else {

				if (mRect.left >= mSpriteSize * 7) {

					//mRect.left = mSpriteSize * 7;
					//setBlocking(false);
				}
				else {
					mRect.left += mSpriteSize;
				}
			}
			mSprite.setTextureRect(mRect);

		}
		else {
			mRect.top = row;
			counter = 0;

		}

		accumulatedTime = 0;
	}


}

void DynamicEntity::deathAnimation(const sf::Time& deltaTime) {
	static double accumulatedTime{ 0 };

	accumulatedTime += deltaTime.asSeconds();

	if (accumulatedTime > DEATH_ANIMATION_DELAY)
	{
		setRectRow(20);
		

		if (mRect.left >= mSpriteSize * 5) {

			mRect.left = mSpriteSize * 5;
		}
		else {
			mRect.left += mSpriteSize;
		}
		
		mSprite.setTextureRect(mRect);
		accumulatedTime = 0;

	}



}


//setter

void DynamicEntity::setRectRow(int row) {
	mRect.top = mSpriteSize * row;
	mSprite.setTextureRect(mRect);
}

void DynamicEntity::setRectCol(int col) {
	mRect.left = mSpriteSize * col;
	mSprite.setTextureRect(mRect);
}

void DynamicEntity::setMoving(bool move) {
	mMoving = move;
}

void DynamicEntity::setBlocking(bool block) {
	mBlock = block;
}

void DynamicEntity::setAttacking(bool attack) {
	mAttack = attack;
}

void DynamicEntity::setStats(std::map<std::string, int> stats) {
	mStats = stats;
}

void DynamicEntity::setPos(double x, double y) {
	mPosX = x;
	mPosY = y;

}

void DynamicEntity::setHitPoint(int hitPoint) {
	mHitPoint = hitPoint;
}

void DynamicEntity::setStamina(int stamina) {
	mStamina = stamina;
}







//getter

int DynamicEntity::getHp() const{
	return mHitPoint;
}



int DynamicEntity::getLevel() const {
	return mLevel;
}


int DynamicEntity::getStamina() const {
	return mStamina;
}

int DynamicEntity::getArmorDefense() const {
	Item *item { mEquipment.at("Armor") };
	Armor *armor{ dynamic_cast<Armor*>(item) };
	int defense{0};

	if (armor) {
		defense = armor->getDefense();
	}

	return defense;
}

int DynamicEntity::getDamage() const {
	Item *item{ mEquipment.at("Weapon") };
	Weapon *weapon{ dynamic_cast<Weapon*>(item) };
	int damage{ 0 };

	if (weapon) {
		damage = weapon->getDamage();
	}
	
	return damage;
}

double DynamicEntity::getSpeed() const {
	return mSpeed;
}

bool DynamicEntity::getBlock() const {
	return mBlock;
}

int DynamicEntity::getBlockValue() const {
	Item *item{ mEquipment.at("SecondaryWeapon") };
	Shield *shield{ dynamic_cast<Shield*>(item) };
	int blockValue{ 0 };

	if (shield) {
		blockValue = shield->getDefense();
	}

	return blockValue;
}

double DynamicEntity::getRange() const{
	return mRange;
}

double DynamicEntity::getVision() const {
	return mVision;
}

sf::IntRect DynamicEntity::getRect() const {
	return mRect;
}

DynamicEntity*  DynamicEntity::getTarget() const {
	return mTarget;
}

std::map<std::string, Item*> DynamicEntity::getEquipment() const {
	return mEquipment;
}

std::map<std::string, int> DynamicEntity::getStats() const {
	return mStats;
}

std::map<std::string, int> DynamicEntity::getStatsCap() const {
	return mStatsCap;
}

std::map<std::string, int> DynamicEntity::getModifier() const {
	return mModifier;
}



