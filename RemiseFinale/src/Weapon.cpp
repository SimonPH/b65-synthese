#include "Weapon.h"


Weapon::Weapon(int level, std::string type, std::string itemName, int durability, int damage, bool oneHanded)
	: Item{ level, type, itemName, durability }, mDamage{ damage }, mOneHanded{ oneHanded }
{
	
}

Weapon::Weapon() {

}

Weapon::~Weapon() {

}

//M�thodes


//getter

int Weapon::getDamage() const {
	return mDamage;
}