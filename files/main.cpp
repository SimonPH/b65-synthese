#include <SFML/Graphics.hpp>
#include <SFML/Window/Keyboard.hpp> // Donne acc�s au clavier en temps r�el
#include "Player.h"
#include "Weapon.h"
#include "Armor.h"
#include "Shield.h"
#include "Monster.h"
#include "TileMap.h"
#include "Ground.h"
#include "InterfaceManager.h"
#include "InputManager.h"
#include "SoundManager.h"
#include "LevelUpMenu.h"
#include "EndGameMenu.h"
#include <cmath>

//AllocConsole()
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <windows.h>


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpCmdLine, int nCmdShow)
{
	AllocConsole();
	freopen("CONOUT$", "w", stdout);
	HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
	int hCrt = _open_osfhandle((long)handle_out, _O_TEXT);
	FILE* hf_out = _fdopen(hCrt, "w");
	setvbuf(hf_out, NULL, _IONBF, 1);
	*stdout = *hf_out;

	HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
	hCrt = _open_osfhandle((long)handle_in, _O_TEXT);
	FILE* hf_in = _fdopen(hCrt, "r");
	setvbuf(hf_in, NULL, _IONBF, 128);
	*stdin = *hf_in;

	// use the console just like a normal one - printf(), getchar(), ...



	const int width = sf::VideoMode::getDesktopMode().width - 100;
	const int height = sf::VideoMode::getDesktopMode().height - 100;

	sf::RenderWindow window(sf::VideoMode(width, height), "RPG");
	// changement de la position de la fen�tre (relativement au bureau)
	window.setPosition(sf::Vector2i(10, 10));
	window.setKeyRepeatEnabled(false); // D�sactive la r�p�tition des �v�nements

	//Contr�le le framerate ()Synchronisation verticale
	//window.setVerticalSyncEnabled(true);
	// pas les deux en m�me temps
	// set le framerate
	window.setFramerateLimit(60);



	time_t srand(time(NULL));
	

	int size{ 64 };
	int row{ 10 };
	int maxCol{ 8 };

	
	//creation de l'equipement de base pour les entit�s
	Weapon baseWeapon(1, "Sword", "Wooden sword", 100, 10, true);
	Weapon *pBaseWeapon{ &baseWeapon };

	Shield baseShield(1, "Light", "Wooden shield", 100, 5);
	Shield *pBaseShield{ &baseShield };

	Armor baseArmor(1, "Medium", "Wooden chest", 100, 5);
	Armor *pBaseArmor{ &baseArmor };

	//ajout de l'equipment dans un map
	std::map<std::string, Item*> equipment = { {"Weapon" ,pBaseWeapon },{ "Armor", pBaseArmor } , {"SecondaryWeapon", pBaseShield }  };

	// chargement des textures
	sf::Texture playerTexture;
	playerTexture.loadFromFile("../images/player.png");
	sf::Texture orcTexture;
	orcTexture.loadFromFile("../images/orc.png");
	sf::Texture skeletonTexture;
	skeletonTexture.loadFromFile("../images/skeleton_invocation.png");
	sf::Texture humanInvokerTexture;
	humanInvokerTexture.loadFromFile("../images/human_invoker.png");
	sf::Texture elfMageTexture;
	elfMageTexture.loadFromFile("../images/elf_mage.png");
	sf::Texture humanHealerTexture;
	humanHealerTexture.loadFromFile("../images/human_healer.png");
	sf::Texture monstersGeneratorTexture;
	monstersGeneratorTexture.loadFromFile("../images/large_headstone.png");
	sf::Texture wallBoulderTexture;
	wallBoulderTexture.loadFromFile("../images/boulder.png");
	sf::Texture fireBallTexture;
	fireBallTexture.loadFromFile("../images/fireball.png");
	sf::Texture lifeOrbTexture;
	lifeOrbTexture.loadFromFile("../images/lifeOrb.png");
	sf::Texture manaOrbTexture;
	manaOrbTexture.loadFromFile("../images/manaOrb.png");
	sf::Texture lifeFoutainTexture;
	lifeFoutainTexture.loadFromFile("../images/lifeFountain.png");
	sf::Texture manaFountainTexture;
	manaFountainTexture.loadFromFile("../images/manaFountain.png");
	sf::Texture emptyFountainTexture;
	emptyFountainTexture.loadFromFile("../images/emptyFountain.png");

	//Creation du gestionnaire de son
	SoundManager soundManager;

	//Cr�ation du terrain
	Ground ground(width, height, window, soundManager, equipment);
	//Ajout de textures pour les entit�s du jeux
	ground.addTexture("player", &playerTexture);
	ground.addTexture("orc", &orcTexture);
	ground.addTexture("skeleton", &skeletonTexture);
	ground.addTexture("large_headstone", &monstersGeneratorTexture);
	ground.addTexture("human_invoker", &humanInvokerTexture);
	ground.addTexture("elf_mage", &elfMageTexture);
	ground.addTexture("human_healer", &humanHealerTexture);
	ground.addTexture("boulder", &wallBoulderTexture);
	ground.addTexture("fireball", &fireBallTexture);
	ground.addTexture("lifeOrb", &lifeOrbTexture);
	ground.addTexture("manaOrb", &manaOrbTexture);
	ground.addTexture("lifeFountain", &lifeFoutainTexture);
	ground.addTexture("manaFountain", &manaFountainTexture);
	ground.addTexture("emptyFountain", &emptyFountainTexture);


	ground.spawnPlayer(equipment);
	ground.spawnMonsters(equipment);
	ground.spawnMonsterGenerator();
	ground.spawnFountains();
	ground.spawnWallDelimiter();

	//Chargement de musique
	ground.getSoundManager().getSound("mainTheme").play();

	//Creation de l'UI
	InterfaceManager interfaceManager(window, &ground);

	//Creation du gestionnaire d'inputs
	InputManager inputManager(window, &ground);

	// on d�finit le niveau � l'aide de num�ro de tuiles
	int w = ceil(width / 32.0);
	int h = ceil(height / 32.0);
	int *level = new int[w*h];
	std::fill(level, level + w*h, 0);
	
	// on cr�e la tilemap avec le niveau pr�c�demment d�fini
	TileMap map;

	//chargement de la texture pour la map
	if(!map.load("../images/stone_walk.png", sf::Vector2u(32, 32), level, w, h)) {
		//return -1;
	}
	
	sf::Time deltaTime; // delta time
	sf::Clock clock;

	//boucle principale d'�venements
	while (window.isOpen())
	{
		//elapsedTime += clock.getElapsedTime();
		deltaTime = clock.getElapsedTime();

		float currentTime = clock.restart().asSeconds();
		float fps = 1.f / currentTime;
		
		//double angle = player.lookOnMouse(window);

		if (!ground.getPlayer()->isAlive()) {
			ground.getSoundManager().getSound("lose").play();
			sf::RenderWindow endGameWindow(sf::VideoMode(500, 200), "Game over ...");
			EndGameMenu endGameMenu = EndGameMenu(endGameWindow, window, ground, equipment, ground.getWidth(), ground.getHeight());
			endGameMenu.event();
		}

		//Regeneration du joueur
		ground.getPlayer()->hitPointRegeneration(deltaTime);
		ground.getPlayer()->staminaRegeneration(deltaTime);

		//gestion des inputs
		inputManager.event();

		inputManager.playerControl();

		//gestion des animations du joueur
		ground.getPlayer()->attackAnimation(deltaTime);
		ground.getPlayer()->blockAnimation(deltaTime);

		//Anime le joueur si il se d�place
		ground.getPlayer()->spriteChanges(8, deltaTime);
		

		//Actions des monstres
		ground.monstersTick(deltaTime);
		ground.manageProjectiles(deltaTime);
		
		
		//UI
		//Affichage

		interfaceManager.changeText(fps);
		

		//Nettoie la surface de dessin
		window.clear();

		//dessine la map
		window.draw(map);

		interfaceManager.drawText();

		ground.draw();


		//Affiche le tout
		window.display();
	}

	return 0;
}