#define _USE_MATH_DEFINES

#include <SFML/Graphics.hpp>


#include <cmath>
#include <math.h>

#include "Player.h"
#include "Item.h"
#include "LevelUpMenu.h"
#include "Ground.h"

Player::Player(int level, std::string name, int hitPoint, int stamina, std::map<std::string, int> stats,
	std::map<std::string, int> statsCap, std::map<std::string, int> modifier, std::map<std::string, Item*> equipment,
	double speed, double posX, double posY, double sizeX, double sizeY, int spriteSize, double range, double vision,
	DynamicEntity *target,  sf::Texture *texture, Ground* ground)
	: DynamicEntity(level, name,hitPoint, stamina, stats, statsCap, modifier, equipment, speed, posX, posY, sizeX, sizeY, spriteSize,
		range, vision, target, texture, ground), mInventory{ {"Gold",Item(1, "Money", "Gold", 0)} }, mExperience{ 0 },
	mExperienceCap{ EXP * level *(1+level)}
{
	

	mSprite.setTexture(*mTexture);

	mRect.left = 0;
	mRect.top = spriteSize * 10;
	mRect.height = spriteSize;
	mRect.width = spriteSize;

	mSprite.setTextureRect(mRect);
}

Player::Player() {


}

Player::~Player() {


}


//M�thodes

void Player::attack() {

	if (enoughEnergy(ATTACK_COST)) {
		mGround->getSoundManager().getSound("melee").stop();
		mGround->getSoundManager().getSound("melee").play();
		mStamina -= ATTACK_COST;
		setAttacking(true);
		setBlocking(false);
		int damage = getDamage() + mModifier["Strength"];

		if (mTarget->isBlocking())
		{
			mTarget->blockAttack(damage);
		}
		else {
			mTarget->armorBlock(damage);

		}

		
		if (!mTarget->isAlive()) {
			receiveExpereience();
			receiveGold();
			checkLevelup();
			mTarget = nullptr;

		}

	}


}

void Player::run() {
	mSpeed *= 1.25;
}

void Player::walk() {
	mSpeed /= 1.25;
}

void Player::move(char direction) {
	if (!isAttacking()) {
		if (!isBlocking()) {
			double tempPosX{ mPosX };
			double tempPosY{ mPosY };

			if (direction == 'R') {
				tempPosX += mSpeed;
				setRectRow(11);

			}
			else if (direction == 'L') {
				tempPosX -= mSpeed;
				setRectRow(9);

			}

			if (direction == 'U') {
				tempPosY -= mSpeed;
				setRectRow(8);

			}
			else if (direction == 'D') {
				tempPosY += mSpeed;
				setRectRow(10);

			}

			
			if (mGround->checkCollision(this, tempPosX, tempPosY)) {
				if (inLimitX(tempPosX)) {
					mPosX = tempPosX;
					setMoving(true);

				}

				if (inLimitY(tempPosY)) {
					mPosY = tempPosY;
					setMoving(true);

				}

			}
			
			
			mSprite.setPosition(mPosX, mPosY);


			
		}
	}

}

void Player::receiveExpereience() {
	mExperience += mTarget->getLevel() * EXP_PER_KILL;

}

void Player::receiveGold() {
	int amount = mInventory["Gold"].getDurability();
	mInventory["Gold"].setDurability(amount + mTarget->getLevel() * MONEY_PER_KILL);
}

void Player::openLevelUpWindow() {
	sf::RenderWindow levelUpWindow(sf::VideoMode(500, 500), "Levelup");
	LevelUpMenu levelUpMenu = LevelUpMenu(levelUpWindow,*this);
	levelUpMenu.event();
}


void Player::checkLevelup() {
	if (mExperience >= mExperienceCap) {
		mLevel += 1;
		mExperienceCap = EXP*mLevel*(1 + mLevel);
		mGround->getSoundManager().getSound("levelup").play();
		openLevelUpWindow();
		setStatsModifier();
		setStatsCap();
		mGround->refillPlayer();


	}
}

//Pourrai eventuellement �tre utilise
/*
double Player::lookOnMouse(sf::RenderWindow &window) {
	sf::Vector2i mousePosition = sf::Mouse::getPosition(window);


	double x{ mousePosition.x - mPosX };
	double y{ mousePosition.y - mPosY };

	//retourne l'arc tangent d'un x et y en radians
	// le - devant atan2 permet de garder (x,y) au lieu de (y,x)
	double angleR{ -atan2(x, y) };
	double angleD{ angleR * (180.0 / M_PI) };
	//double theta { -atan2(x, y)};
	//double angle { std::fmod((90.0 - ((theta * 180.0) / M_PI)) , 360.0)};
	//double angle { (180.0 / M_PI) * theta};


	

	
	if (angle > 0 && angle < 500) {
		setRectRow(8);
		setRectCol(0);
	}
	else if (angle > 225 && angle < 315) {
		setRectRow(9);
		setRectCol(0);
	}
	else if (angle > 135 && angle < 225) {
		setRectRow(10);
		setRectCol(0);
	}
	else if (angle > 45 && angle < 135) {
		setRectRow(11);
		setRectCol(0);
	}
	
	
	

	mSprite.setTextureRect(mRect);

	return angleD;
	//sprite.setRotation(angle);
}
*/

void Player::inRangeObject() const {

	for (auto &target : mGround->getObjects()) {

		if (mPosX < target->getPosX()) {
			if (mPosX + mRange >= target->getPosX()) {
				if (mPosY < target->getPosY()) {
					if (mPosY + mRange >= target->getPosY()) {
						target->action();
					}
				}
				else {
					if (mPosY - mRange <= target->getPosY()) {
						target->action();

					}
				}
			}
		}
		else {
			if (mPosX - mRange <= target->getPosX()) {
				if (mPosY < target->getPosY()) {
					if (mPosY + mRange >= target->getPosY()) {
						target->action();

					}
				}
				else {
					if (mPosY - mRange <= target->getPosY()) {
						target->action();

					}
				}
			}
		}
	}




}


//getter
DynamicEntity* Player::getTarget() const {
	return mTarget;
}

int Player::getExperience() const {
	return mExperience;
}

int Player::getExperienceCap() const {
	return mExperienceCap;
}

//setter
void Player::abandonTarget() {
	mTarget = nullptr;
}

void Player::setTarget(Monster *potentialTarget) {
	mTarget = potentialTarget;
}




