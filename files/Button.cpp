#include "Button.h"

Button::Button(std::string text, sf::Vector2f size, int posX, int posY)
{
	
	mText.setString(text);

	mBackground.setOrigin(0, 0);
	mBackground.setSize(size);
	mBackground.setPosition(posX, posY);

	mText.setPosition(mBackground.getPosition());
}

sf::RectangleShape Button::getBackground() {
	return mBackground;
}

sf::Text Button::getText() {
	return mText;
}


void Button::setBackgroundColor(sf::Color color, sf::Color borderColor) {
	mBackground.setFillColor(color);
	mBackground.setOutlineColor(borderColor);
	mBackground.setOutlineThickness(5);
}

void Button::setFont(sf::Font& font, int size, sf::Color color)
{
	mText.setFont(font);
	mText.setColor(color);
	mText.setStyle(sf::Text::Bold);
	mText.setCharacterSize(size);
}

bool Button::isClick(sf::Vector2i mousePosition)
{
	if (mousePosition.x >= mBackground.getGlobalBounds().left && mousePosition.x <= (mBackground.getGlobalBounds().left + mBackground.getGlobalBounds().width)
		&& mousePosition.y >= mBackground.getGlobalBounds().top && mousePosition.y <= (mBackground.getGlobalBounds().top + mBackground.getGlobalBounds().height))
	{
		return true;
	}
	else
	{
		return false;
	}
}
